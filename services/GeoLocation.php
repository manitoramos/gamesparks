<?php

add_action("admin_init", "geolocation_custom_metabox");

function geolocation_custom_metabox( ){
    add_meta_box( "geolocation", "GeoLocation ID", "geolocation_metabox", "geolocation", "side", "low" );
}

function geolocation_metabox(){
    global $post;

    $data = get_post_custom( $post->ID );
    $val = isset( $data['geolocation_id'] ) ? esc_attr( $data['geolocation_id'][0] ) : null;
    echo "<small>Automatic Generated after publish the post</small>";
    echo "<input type='text' name='geolocation_id' id='geolocation_id' style='width:100%;' value='{$val}' disabled />";
}


add_action( "save_post_geolocation","save_geolocation" );

function save_geolocation( ){
    global $post;
    global $trashpost;

    //on untrash didnt make the post function
    if( isset($_GET['action']) && $_GET['action'] == 'untrash' ){
      $previous = "javascript:history.go(-1)";
      if(isset($_SERVER['HTTP_REFERER'])) {
          $previous = $_SERVER['HTTP_REFERER'];
      }
      header('Location: '.$previous);
      return;
    }

    if($trashpost == 1){
      return;
    }

    static $updated = false;

    if( $updated ) {
      return;
    }

    //if the post is not created yet
    if( !isset( $post ) ){
      return;
    }

    if( define('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
      return $post->ID;
    }
    if( isset( $post->ID ) ){

        $updated = true;

        new GeoLocation();

    }
}


/**
 * GameSparks
 */
class GeoLocation
{
  private $geoID;
  private $geoName;
  private $latitude;
  private $longitude;

  function __construct()
  {
    global $post;

    //values to vars
    $this->geoName  = $post->post_title;
    $this->latitude = get_post_meta( $post->ID, 'latitude', true );
    $this->longitude= get_post_meta( $post->ID, 'longitude', true );

    if( get_post_meta( $post->ID, 'geolocation_id', true ) !== null && get_post_meta( $post->ID, 'geolocation_id', true ) !== "" )
    {
      $this->geoID = get_post_meta( $post->ID, 'geolocation_id', true );

      //update geolocation
      $this->UpdateGeoLocation();
    }else{//create
      update_post_meta( $post->ID, 'geolocation_id', $this->CreateGeoLocation() );
      exit();
    }

  }

  /**
  **
  **/
  private function CreateGeoLocation()
  {
    $ch = curl_init();

    $params = '{
      "@class": ".LogEventRequest",
      "eventKey": "CreateGeoLocation",
      "geoLocationName": "'. $this->geoName .'",
      "latitude": '. floatval($this->latitude) .',
      "longitude": '. floatval($this->longitude) .',
      "playerId": "'.playerID.'"
    }';

    curl_setopt($ch, CURLOPT_URL, "https://w373975yvdzr.preview.gamesparks.net/rs/device/MkfEiAZ5KZVUSpiohZF3g3GYr8iWXZFf/LogEventRequest");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_POST, 1);

    $headers = array();
    $headers[] = "Content-Type: application/json";
    $headers[] = "Accept: application/json";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }else{
      $this->Logs($result);
      $result = json_decode($result);
    }
    curl_close ($ch);
    if( isset($result->scriptData->new_geoLocation_ID) ){
      $this->DataBase('insert');
      $this->geoID = intval($result->scriptData->new_geoLocation_ID);
      return $result->scriptData->new_geoLocation_ID;
    }
  }

  /**
  **
  **/
  private function UpdateGeoLocation()
  {
    try {
      $ch = curl_init();

      $params = '{
        "@class": ".LogEventRequest",
        "eventKey": "EditGeoLocation",
        "geoLocationID": "'. $this->geoID .'",
        "geoLocationName": "'. $this->geoName .'",
        "latitude": '. floatval($this->latitude) .',
        "longitude": '. floatval($this->longitude) .',
        "playerId": "'.playerID.'"
      }';

      curl_setopt($ch, CURLOPT_URL, "https://w373975yvdzr.preview.gamesparks.net/rs/device/MkfEiAZ5KZVUSpiohZF3g3GYr8iWXZFf/LogEventRequest");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      curl_setopt($ch, CURLOPT_POST, 1);

      $headers = array();
      $headers[] = "Content-Type: application/json";
      $headers[] = "Accept: application/json";
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      $result = curl_exec($ch);
      if (curl_errno($ch)) {
          echo 'Error:' . curl_error($ch);
          $this->Logs(curl_error($ch));
      }else{
        //return isset($result->scriptData->update)
      }
      curl_close ($ch);
      $this->Logs($result);
      $result = json_decode($result);
      $this->DataBase("update");
    } catch (Exception $e) {
      $this->Logs($e);
    }
  }

  /**
  **
  **/
  private function DataBase( $task )
  {
    global $wpdb;

    if( $task == "insert" ){
      $wpdb->query( $wpdb->prepare( "INSERT INTO GeoLocation (geoLocationID,geoLocationName,latitude,longitude,dateTimeCreated,dateTimeModified)
                                         VALUES ('%d','%s','%s','%s','%s','%s')",
                                         array($this->geoID,$this->geoName,$this->latitude,$this->longitude,date('Y-m-d H:i:s'),date('Y-m-d H:i:s')) ) );
    }else if( $task == "update" ){
      //check if exists on database otherwise insert again
      $res = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM GeoLocation WHERE geoLocationID = %d",array($this->geoID) ) );

      if( sizeof($res) > 0 ){
        $wpdb->get_results( $wpdb->prepare( "UPDATE GeoLocation
                                             SET geoLocationName = '%s', latitude = '%s', longitude = '%s', dateTimeModified = '%s'
                                             WHERE geoLocationID = %d",array($this->geoName,$this->latitude,$this->longitude,date('Y-m-d H:i:s'),$this->geoID) ) );
      }else{
        $wpdb->query( $wpdb->prepare( "INSERT INTO GeoLocation (geoLocationID,geoLocationName,latitude,longitude,dateTimeCreated,dateTimeModified)
                                           VALUES ('%d','%s','%s','%s','%s','%s')",
                                           array($this->geoID,$this->geoName,$this->latitude,$this->longitude,date('Y-m-d H:i:s'),date('Y-m-d H:i:s')) ) );
      }

    }

  }

  /**
  ** Logs from gamesparks api
  **/
  private function Logs($request)
  {
    global $wpdb;

    $wpdb->query( "INSERT INTO Gamespark_logs (name,log,date) VALUES ('geoLocation','{$request}',NOW())" );

  }
}
