<?php
/****************
**   Company   **
*****************/
add_action("admin_init", "custom_metabox");

function custom_metabox( ){
    add_meta_box( "custom_metabox_0111", "Company ID", "custom_metabox_field", "company", "side", "low" );
}

function custom_metabox_field(){
    global $post;

    $data = get_post_custom( $post->ID );
    $val = isset( $data['company_id'] ) ? esc_attr( $data['company_id'][0] ) : null;
    echo "<small>Automatic Generated after publish the post</small>";
    echo "<input type='text' name='company_id' id='company_id' style='width:100%;' value='{$val}' disabled />";
    //echo "&nbsp;<button id='savepost' style='float:right;margin-top:10px;' class='button button-primary button-large'>Save Post</button>";
}

/****************
**  Campaign   **
*****************/
add_action("admin_init", "campaign_custom_metabox");

function campaign_custom_metabox( ){
    add_meta_box( "campaign", "Campaign ID", "campaign_metabox", "campaign", "side", "low" );
}

function campaign_metabox(){
    global $post;

    $data = get_post_custom( $post->ID );
    $val = isset( $data['campaign_id'] ) ? esc_attr( $data['campaign_id'][0] ) : null;
    echo "<small>Automatic Generated after publish the post</small>";
    echo "<input type='text' name='campaign_id' id='campaign_id' style='width:100%;' value='{$val}' disabled />";
}

/****************
** GeoLocation **
*****************/
add_action("admin_init", "geolocation_custom_metabox");

function geolocation_custom_metabox( ){
    add_meta_box( "geolocation", "GeoLocation ID", "geolocation_metabox", "geolocation", "side", "low" );
}

function geolocation_metabox(){
    global $post;

    $data = get_post_custom( $post->ID );
    $val = isset( $data['geolocation_id'] ) ? esc_attr( $data['geolocation_id'][0] ) : null;
    echo "<small>Automatic Generated after publish the post</small>";
    echo "<input type='text' name='geolocation_id' id='geolocation_id' style='width:100%;' value='{$val}' disabled />";
}

/****************
** Redemption  **
*****************/
add_action("admin_init", "redemption_custom_metabox");

function redemption_custom_metabox( ){
    add_meta_box( "redemption", "Redemption ID", "redemption_metabox", "redemptionlocation", "side", "low" );
}

function redemption_metabox(){
    global $post;

    $data = get_post_custom( $post->ID );
    $val = isset( $data['redemption_id'] ) ? esc_attr( $data['redemption_id'][0] ) : null;
    echo "<small>Automatic Generated after publish the post</small>";
    echo "<input type='text' name='redemption_id' id='redemption_id' style='width:100%;' value='{$val}' disabled />";
}

/****************
**    Dropt    **
*****************/
add_action("admin_init", "dropt_custom_metabox");

function dropt_custom_metabox( ){
    add_meta_box( "dropt", "Dropt ID", "dropt_metabox", "dropt", "side", "low" );
}

function dropt_metabox(){
    global $post;

    $data = get_post_custom( $post->ID );
    $val = isset( $data['dropt_id'] ) ? esc_attr( $data['dropt_id'][0] ) : null;
    echo "<small>Automatic Generated after publish the post</small>";
    echo "<input type='text' name='dropt_id' id='dropt_id' style='width:100%;' value='{$val}' disabled />";
}

/****************
**    Prize    **
*****************/
add_action("admin_init", "prize_custom_metabox");

function prize_custom_metabox( ){
    add_meta_box( "prize", "Prize ID", "prize_metabox", "prize", "side", "low" );
}

function prize_metabox(){
    global $post;

    $data = get_post_custom( $post->ID );
    $val = isset( $data['prize_id'] ) ? esc_attr( $data['prize_id'][0] ) : null;
    echo "<small>Automatic Generated after publish the post</small>";
    echo "<input type='text' name='prize_id' id='prize_id' style='width:100%;' value='{$val}' disabled />";
}

/**
** group ID
**/
add_action("admin_init", "groupid_metabox");

function groupid_metabox( ){
    add_meta_box( "groupid", "Group NAME / ID", "group_metabox", "prize", "normal", "high" );
}

function group_metabox(){
    global $post;
    global $wpdb;

    $res = $wpdb->get_results("SELECT * FROM Groups WHERE groupID > 0 GROUP BY groupID");

    $data = get_post_custom( $post->ID );
    $val = isset( $data['group_id'] ) ? esc_attr( $data['group_id'][0] ) : null;
    //echo "<select type='text' name='prize_id' id='prize_id' style='width:100%;' value='{$val}'/>";
    echo "<select id='group_id' name='group_id' style='width:100%;'>";
    for( $i = 0; $i < sizeof($res); $i++ ){
      if( $res[$i]->groupID == $val ){
          echo "<option value='{$res[$i]->groupID}' selected>{$res[$i]->groupName} / {$res[$i]->groupID}</option>";
      }else{
          echo "<option value='{$res[$i]->groupID}'>{$res[$i]->groupName} / {$res[$i]->groupID}</option>";
      }
    }
    echo "</select>";
}
