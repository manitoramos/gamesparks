<?php

/**
 * GameSparks
 */
class Redemption extends ControllerSparks
{
    private $redemptionID;
    private $locationName;
    private $companyID;
    private $phoneNumber;
    private $street1;
    private $street2;
    private $city;
    private $state;
    private $zipCode;
    private $radiusinMiles;
    private $geoLocationID;

    function __construct()
    {
        $this->Configs( "prod" );

        global $post,$wpdb;

        //get the title
        $title = $wpdb->get_results("select * from {$wpdb->prefix}posts where ID = {$post->ID}");
        $this->locationName = $title[0]->post_title;
        //values to vars
        $this->companyID = $this->GetCompanyID();
        $this->phoneNumber = get_field('phonenumber');
        $this->street1 = get_field('street1');
        $this->street2 = get_field('street2');
        $this->city = get_field('city');
        $this->state = get_field('state');
        $this->zipCode = get_field('zipCode');
        $this->radiusinMiles = get_field('radiusInMiles');
        $this->geoLocationID = $this->GetGeoLocationID();
        //$this->geoLocationID= explode('"', get_post_meta( $post->ID, 'geoLocation_redemption_location', true ))[1];

        if (get_field('redemption_id') !== null && get_field('redemption_id') !== "") {
            $this->redemptionID = get_field('redemption_id');

            //update geolocation
            $this->UpdateRedemption();
        } else {//create
            update_post_meta($post->ID, 'redemption_id', $this->CreateRedemption());
            //exit();
        }

    }

    /**
     **
     **/
    private function CreateRedemption()
    {
        global $post;

        $ch = curl_init();

        $params = '{
          "@class": ".LogEventRequest",
          "eventKey": "CreateRedemptionLocation",
          "companyID": "' . $this->companyID . '",
          "locationName": "' . $this->locationName . '",
          "phoneNumber": "' . $this->phoneNumber . '",
          "street1": "' . $this->street1 . '",
          "street2": "' . $this->street2 . '",
          "city": "' . $this->city . '",
          "state": "' . $this->state . '",
          "zipCode": "' . $this->zipCode . '",
          "radiusInMiles": "' . $this->radiusinMiles . '",
          "geoLocationID": "' . $this->geoLocationID . '",
          "playerId": "' . playerID . '"
        }';

        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        $headers[] = "Content-Type: application/json";
        $headers[] = "Accept: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        } else {
            $this->Logs($result);
            update_post_meta($post->ID, 'box_log', $result);
            update_post_meta( $post->ID, 'query_log', $params );
            $result = json_decode($result);
        }
        curl_close($ch);
        if (isset($result->scriptData->new_redemption_location_ID)) {
            $this->redemptionID = intval($result->scriptData->new_redemption_location_ID);
            $this->DataBase('insert');
            return $result->scriptData->new_redemption_location_ID;
        }else{
            $my_post = array(
                'ID'           => $post->ID,
                'post_status'   => 'error',
            );
            wp_update_post( $my_post );
        }
    }

    /**
     **
     **/
    private function UpdateRedemption()
    {
        global $post,$wpdb;

        try {
            $ch = curl_init();

            $params = '{
              "@class": ".LogEventRequest",
              "eventKey": "EditRedemptionLocation",
              "redemptionID": "' . $this->redemptionID . '",
              "companyID": "' . $this->companyID . '",
              "locationName": "' . $this->locationName . '",
              "phoneNumber": "' . $this->phoneNumber . '",
              "street1": "' . $this->street1 . '",
              "street2": "' . $this->street2 . '",
              "city": "' . $this->city . '",
              "state": "' . $this->state . '",
              "zipCode": "' . $this->zipCode . '",
              "radiusInMiles": "' . $this->radiusinMiles . '",
              "geoLocationID": "' . $this->geoLocationID . '",
              "playerId": "' . playerID . '"
            }';

            curl_setopt($ch, CURLOPT_URL, $this->url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_POST, 1);

            $headers = array();
            $headers[] = "Content-Type: application/json";
            $headers[] = "Accept: application/json";
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
                $this->Logs(curl_error($ch));
            } else {
                $this->Logs($result);
                update_post_meta($post->ID, 'box_log', $result);
                update_post_meta( $post->ID, 'query_log', $params );
                $result = json_decode($result);
                //return isset($result->scriptData->update)
            }
            curl_close($ch);
            if( $result->scriptData->update == "success" ){
                $this->DataBase("update");
            } else{
                $res = $wpdb->get_results( "SELECT * FROM RedemptionLocation WHERE redemptionID = $this->redemptionID" );
                //on error put the data to the same old value
                update_post_meta( $post->ID, 'redemption_id', $this->redemptionID );
                update_post_meta( $post->ID, 'company_redemption_location', $res[0]->companyID );
                update_post_meta( $post->ID, 'phonenumber', $res[0]->phoneNumber );
                update_post_meta( $post->ID, 'street1', $res[0]->street1 );
                update_post_meta( $post->ID, 'street2', $res[0]->street2 );
                update_post_meta( $post->ID, 'state', $res[0]->state );
                update_post_meta( $post->ID, 'zipCode', $res[0]->zipCode );
                update_post_meta( $post->ID, 'city', $res[0]->city );
                update_post_meta( $post->ID, 'radiusInMiles', $res[0]->radiusinMiles );
                update_post_meta( $post->ID, 'geoLocation_redemption_location', $res[0]->geoLocationID );
                wp_update_post( array('ID' => $post->ID, 'post_title' => "{$res[0]->locationName}" ) );
            }
        } catch (Exception $e) {
            $this->Logs($e);
        }
    }

    /**
     **
     **/
    private function DataBase($task)
    {
        global $wpdb;

        if ($task == "insert") {
            $wpdb->query($wpdb->prepare("INSERT INTO RedemptionLocation (redemptionID,companyID,locationName,phoneNumber,street1,street2,city,
                                       state,zipCode,radiusinMiles,geoLocationID,dateTimeCreated,dateTimeModified)
                                       VALUES ('%d','%d','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                array($this->redemptionID, $this->companyID, $this->locationName, $this->phoneNumber, $this->street1,
                    $this->street2, $this->city, $this->state, $this->zipCode, $this->radiusinMiles, $this->geoLocationID, date('Y-m-d H:i:s'), date('Y-m-d H:i:s'))));
            if ($wpdb->last_error !== '') {
                $this->Logs($wpdb->last_result());
                $this->Logs($wpdb->last_error());
            }
        } else if ($task == "update") {
            //check if exists on database otherwise insert again
            $res = $wpdb->get_results($wpdb->prepare("SELECT * FROM RedemptionLocation WHERE redemptionID = %d", array($this->redemptionID)));

            if (sizeof($res) > 0) {
                $wpdb->get_results($wpdb->prepare("UPDATE RedemptionLocation
                                             SET companyID = '%d', locationName = '%s', phoneNumber = '%s', street1 = '%s', street2 = '%s', city = '%s', state = '%s',
                                             zipCode = '%s', radiusinMiles = '%s', geoLocationID = '%s', dateTimeModified = '%s'
                                             WHERE redemptionID = %d",
                    array($this->companyID, $this->locationName, $this->phoneNumber, $this->street1, $this->street2, $this->city, $this->state, $this->zipCode,
                        $this->radiusinMiles, $this->geoLocationID, date('Y-m-d H:i:s'), $this->redemptionID)));
            } else {
                $wpdb->query($wpdb->prepare("INSERT INTO RedemptionLocation (redemptionID,companyID,locationName,phoneNumber,street1,street2,city,
                                       state,zipCode,radiusinMiles,geoLocationID,dateTimeCreated,dateTimeModified)
                                       VALUES ('%d','%d','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                    array($this->redemptionID, $this->companyID, $this->locationName, $this->phoneNumber, $this->street1,
                        $this->street2, $this->city, $this->state, $this->zipCode, $this->radiusinMiles, $this->geoLocationID, date('Y-m-d H:i:s'), date('Y-m-d H:i:s'))));
            }

        }

    }

    /**
     ** GET COMPANY ID
     **/
    private function GetCompanyID()
    {

        global $wpdb;
        global $post;

        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}postmeta WHERE post_id = {$post->ID} AND meta_key = 'company_redemption_location'");

        $value = $res[0]->meta_value;

        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}postmeta WHERE post_id = {$value} AND meta_key = 'company_id'");

        return $res[0]->meta_value;

    }

    /**
     ** GET GEOLOCTION ID
     **/
    private function GetGeoLocationID()
    {

        global $wpdb;
        global $post;

        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}postmeta WHERE post_id = {$post->ID} AND meta_key = 'geoLocation_redemption_location'");

        $value = $res[0]->meta_value;

        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}postmeta WHERE post_id = {$value} AND meta_key = 'geolocation_id'");

        return $res[0]->meta_value;

    }

    /**
     ** Logs from gamesparks api
     **/
    private function Logs($request)
    {
        global $wpdb;

        $wpdb->query("INSERT INTO Gamespark_logs (name,log,date) VALUES ('redemption','{$request}',NOW())");

    }
}
