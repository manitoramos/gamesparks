<?php

/****************
**   Company   **
*****************/
add_action("admin_init", "log_metabox");

function log_metabox( ){
    add_meta_box( "error_custom_metabox", "Last Log (Debug only)", "custom_log", "company", "side", "low" );
}
/****************
**  Campaign   **
*****************/
add_action("admin_init", "log_metabox_camp");

function log_metabox_camp( ){
    add_meta_box( "error_custom_camp", "Last Log (Debug only)", "custom_log", "campaign", "side", "low" );
}
/****************
** GeoLocation **
*****************/
add_action("admin_init", "log_metabox_geo");

function log_metabox_geo( ){
    add_meta_box( "error_custom_geo", "Last Log (Debug only)", "custom_log", "geolocation", "side", "low" );
}
/****************
** Redemption  **
*****************/
add_action("admin_init", "log_metabox_red");

function log_metabox_red( ){
    add_meta_box( "error_custom_red", "Last Log (Debug only)", "custom_log", "redemptionlocation", "side", "low" );
}
/****************
**    Dropt    **
*****************/
add_action("admin_init", "log_metabox_dropt");

function log_metabox_dropt( ){
    add_meta_box( "error_custom_dropt", "Last Log (Debug only)", "custom_log", "dropt", "side", "low" );
}
/****************
**    Prize    **
*****************/
add_action("admin_init", "log_metabox_prize");

function log_metabox_prize( ){
    add_meta_box( "error_custom_prize", "Last Log (Debug only)", "custom_log", "prize", "side", "low" );
}

/**
**
**/
function custom_log(){
    global $post;

    $data = get_post_custom( $post->ID );
    $val = isset( $data['box_log'] ) ? esc_attr( $data['box_log'][0] ) : null;
    echo "<div style='color:black;'>{$val}</div>";
}


/*********************************************************
**
**                  QUERY LAST LOG
**
*********************************************************/

/****************
**   Company   **
*****************/
add_action("admin_init", "query_metabox");

function query_metabox( ){
    add_meta_box( "query_custom_metabox", "Query Log (Debug only)", "query_log", "company", "side", "low" );
}
/****************
**  Campaign   **
*****************/
add_action("admin_init", "query_metabox_camp");

function query_metabox_camp( ){
    add_meta_box( "query_custom_camp", "Query Log (Debug only)", "query_log", "campaign", "side", "low" );
}
/****************
** GeoLocation **
*****************/
add_action("admin_init", "query_metabox_geo");

function query_metabox_geo( ){
    add_meta_box( "query_custom_geo", "Query Log (Debug only)", "query_log", "geolocation", "side", "low" );
}
/****************
** Redemption  **
*****************/
add_action("admin_init", "query_metabox_red");

function query_metabox_red( ){
    add_meta_box( "query_custom_red", "Query Log (Debug only)", "query_log", "redemptionlocation", "side", "low" );
}
/****************
**    Dropt    **
*****************/
add_action("admin_init", "query_metabox_dropt");

function query_metabox_dropt( ){
    add_meta_box( "query_custom_dropt", "Query Log (Debug only)", "query_log", "dropt", "side", "low" );
}
/****************
**    Prize    **
*****************/
add_action("admin_init", "query_metabox_prize");

function query_metabox_prize( ){
    add_meta_box( "query_custom_prize", "Query Log (Debug only)", "query_log", "prize", "side", "low" );
}

/**
**
**/
function query_log(){
    global $post;

    $data = get_post_custom( $post->ID );
    $val = isset( $data['query_log'] ) ? esc_attr( $data['query_log'][0] ) : null;
    echo "<div style='color:black;'>{$val}</div>";
}
