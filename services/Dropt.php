<?php

add_action("admin_init", "dropt_custom_metabox");

function dropt_custom_metabox( ){
    add_meta_box( "dropt", "Dropt ID", "dropt_metabox", "dropt", "side", "low" );
}

function dropt_metabox(){
    global $post;

    $data = get_post_custom( $post->ID );
    $val = isset( $data['dropt_id'] ) ? esc_attr( $data['dropt_id'][0] ) : null;
    echo "<small>Automatic Generated after publish the post</small>";
    echo "<input type='text' name='dropt_id' id='dropt_id' style='width:100%;' value='{$val}' disabled />";
}


function save_dropt( ){
    global $post;
    global $trashpost;

    //on untrash didnt make the post function
    if( isset($_GET['action']) && $_GET['action'] == 'untrash' ){
      $previous = "javascript:history.go(-1)";
      if(isset($_SERVER['HTTP_REFERER'])) {
          $previous = $_SERVER['HTTP_REFERER'];
      }
      header('Location: '.$previous);
      return;
    }

    if($trashpost == 1){
      return;
    }

    static $updated = false;

    if( $updated ) {
      return;
    }

    //if the post is not created yet
    if( !isset( $post ) ){
      return;
    }

    // if( define('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
    //   return $post->ID;
    // }
    if( isset( $post->ID ) ){

        $updated = true;

        //new Dropt();

    }
}


/**
 * GameSparks
 */
class Dropt
{
  //main vars
  private $droptID;
  private $companyID;
  private $campaignID;
  private $groupName;
  private $droptName;
  private $redemptionIDs;
  private $termsAndConditions;
  private $singleDroptRule;
  private $groupRules;
  private $visibleUserEmails;
  private $dateTimeCaptureBegin;
  private $dateTimeCaptureEnd;
  private $dateTimeRedeemBegin;
  private $dateTimeRedeemEnd;
  private $droptImageURLs;
  private $geoLocationIDs;
  private $radiusinMiles;
  private $duplicateThisMany;

  //image vars
  private $offerCodes;
  private $parachutePayload;
  private $parachuteOffer;
  private $capturedSuccess;
  private $whatsDroppinOffer;
  private $walletIcon;
  private $contestHeaderOffer;
  private $contestHeaderIcon;
  private $contestProgressIcon;


  function __construct()
  {
    global $post;

    //values to vars
    $this->companyID = get_post_meta( $post->ID, 'company_dropt', true );
    $this->groupName= get_post_meta( $post->ID, 'groupName', true );
    $this->campaignID= get_post_meta( $post->ID, 'campaign_dropt', true );
    $this->redemptionIDs= get_post_meta( $post->ID, 'redemptionLocationIDs_dropt', true );
    $this->termsAndConditions= get_post_meta( $post->ID, 'termsAndConditions', true );
    $this->singleDroptRule= get_post_meta( $post->ID, 'singleDroptRules', true );
    $this->groupRules= get_post_meta( $post->ID, 'groupRules', true );
    $this->visibleUserEmails= get_post_meta( $post->ID, 'visibleUserEmails', true );
    $this->dateTimeCaptureBegin= get_post_meta( $post->ID, 'dateTimeCaptureBegin', true );
    $this->dateTimeCaptureEnd= get_post_meta( $post->ID, 'dateTimeCaptureEnd', true );
    $this->dateTimeRedeemBegin= get_post_meta( $post->ID, 'dateTimeRedeemBegin', true );
    $this->dateTimeRedeemEnd= get_post_meta( $post->ID, 'dateTimeRedeemEnd', true );
    $this->droptImageURLs= get_post_meta( $post->ID, 'droptImagesURLs', true );
    $this->geoLocationIDs= get_post_meta( $post->ID, 'geoLocation_dropt', true );
    $this->radiusinMiles= get_post_meta( $post->ID, 'radiusInMiles', true );
    $this->duplicateThisMany= get_post_meta( $post->ID, 'duplicateThisMany', true );

    //images vars
    $this->offerCodes = get_post_meta( $post->ID, 'offerCodes', true );
    $this->parachutePayload = get_post_meta( $post->ID, 'parachutePayload', true );
    $this->parachuteOffer = get_post_meta( $post->ID, 'parachuteOffer', true );
    $this->capturedSuccess = get_post_meta( $post->ID, 'capturedSuccess', true );
    $this->whatsDroppinOffer = get_post_meta( $post->ID, 'whatsDroppinOffer', true );
    $this->walletIcon = get_post_meta( $post->ID, 'walletIcon', true );
    $this->contestHeaderOffer = get_post_meta( $post->ID, 'contestHeaderOffer', true );
    $this->contestHeaderIcon = get_post_meta( $post->ID, 'contestHeaderIcon', true );
    $this->contestProgressIcon = get_post_meta( $post->ID, 'contestProgressIcon', true );

    if( get_post_meta( $post->ID, 'dropt_id', true ) !== null && get_post_meta( $post->ID, 'dropt_id', true ) !== "" )
    {
      $this->droptID = get_post_meta( $post->ID, 'dropt_id', true );

      //update dropt
      $this->UpdateDropt();
    }else{//create
      update_post_meta( $post->ID, 'dropt_id', $this->CreateDropt() );

    }

  }

  /**
  **
  **/
  private function CreateDropt()
  {
    $ch = curl_init();

    $params = '{
      "@class": ".LogEventRequest",
      "sponsorCompanyID": "'. $this->companyID .'",
      "campaignID": "'. $this->campaignID .'",
      "groupName": "'. $this->groupName .'",
      "droptName": "'. $this->droptName .'",
      "redemptionLocationIDs": "'. $this->redemptionIDs .'",
      "termsAndConditions": "'. $this->termsAndConditions .'",
      "singleDroptRule": "'. $this->singleDroptRule .'",
      "groupRules": "'. $this->groupRules .'",
      "visibleUserEmails": "'. $this->visibleUserEmails .'",
      "dateTimeCaptureBegin": "'. $this->dateTimeCaptureBegin .'",
      "dateTimeCaptureEnd": "'. $this->dateTimeCaptureEnd .'",
      "dateTimeRedeemBegin": "'. $this->dateTimeRedeemBegin .'",
      "dateTimeRedeemEnd": "'. $this->dateTimeCaptureEnd .'",
      "droptImageURLs": {
        "offerCodes": "'. $this->offerCodes .'",
        "parachutePayload": "'. $this->parachutePayload .'",
        "parachuteOffer": "'. $this->parachuteOffer .'",
        "capturedSuccess": "'. $this->capturedSuccess .'",
        "whatsDroppinOffer": "'. $this->whatsDroppinOffer .'",
        "walletIcon": "'. $this->walletIcon .'",
        "contestHeaderOffer": "'. $this->contestHeaderOffer .'",
        "contestHeaderIcon": "'. $this->contestHeaderIcon .'",
        "contestProgressIcon": "'. $this->contestProgressIcon .'"
      },
      "geoLocationIDs": "'. $this->geoLocationIDs .'",
      "radiusinMiles": "'. $this->radiusinMiles .'",
      "duplicateThisMany": "'. $this->duplicateThisMany .'",
      "playerId": "'.playerID.'"
    }';

    curl_setopt($ch, CURLOPT_URL, "https://w373975yvdzr.preview.gamesparks.net/rs/device/MkfEiAZ5KZVUSpiohZF3g3GYr8iWXZFf/LogEventRequest");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_POST, 1);

    $headers = array();
    $headers[] = "Content-Type: application/json";
    $headers[] = "Accept: application/json";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }else{
      $this->Logs($result);
      $result = json_decode($result);
    }
    curl_close ($ch);
    if( isset($result->scriptData->new_campaign_ID) ){
      $this->DataBase('insert');
      $this->campaignID = intval($result->scriptData->new_campaign_ID);
      return $result->scriptData->new_campaign_ID;
    }
  }

  /**
  **newDroptID
  **/
  private function UpdateDropt()
  {
    try {
      $ch = curl_init();

      $params = '{
        "@class": ".LogEventRequest",
        "eventKey": "EditCampaign",
        "campaignID": "'. $this->campaignID .'",
        "companyID": "'. 2 .'",
        "globalGroupRule": "'. $this->rule .'",
        "playerId": "'.playerID.'"
      }';

      curl_setopt($ch, CURLOPT_URL, "https://w373975yvdzr.preview.gamesparks.net/rs/device/MkfEiAZ5KZVUSpiohZF3g3GYr8iWXZFf/LogEventRequest");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      curl_setopt($ch, CURLOPT_POST, 1);

      $headers = array();
      $headers[] = "Content-Type: application/json";
      $headers[] = "Accept: application/json";
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      $result = curl_exec($ch);
      if (curl_errno($ch)) {
          echo 'Error:' . curl_error($ch);
          $this->Logs(curl_error($ch));
      }else{
        //return isset($result->scriptData->update)
      }
      curl_close ($ch);
      $this->Logs($result);
      $result = json_decode($result);
      $this->DataBase("update");
    } catch (Exception $e) {
      $this->Logs($e);
    }
  }

  /**
  **
  **/
  private function DataBase( $task )
  {
    global $wpdb;

    if( $task == "insert" ){
      $wpdb->query( $wpdb->prepare( "INSERT INTO Campaign (campaignID,companyID,globalGroupRule,dateTimeCreated,dateTimeModified)
                                         VALUES ('%d','%d','%s','%s','%s')",
                                         array($this->campaignID,$this->companyID,$this->globalGroupRule,date('Y-m-d H:i:s'),date('Y-m-d H:i:s')) ) );
    }else if( $task == "update" ){
      //check if exists on database otherwise insert again
      $res = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM Campaign WHERE campaignID = %d",array($this->geoID) ) );

      if( sizeof($res) > 0 ){
        $wpdb->get_results( $wpdb->prepare( "UPDATE Campaign
                                             SET companyID = '%d', globalGroupRule = '%s', dateTimeModified = '%s'
                                             WHERE campaignID = %d",array($this->companyID,$this->globalGroupRule,date('Y-m-d H:i:s'),$this->campaignID) ) );
      }else{
        $wpdb->query( $wpdb->prepare( "INSERT INTO Campaign (campaignID,companyID,globalGroupRule,dateTimeCreated,dateTimeModified)
                                           VALUES ('%d','%d','%s','%s','%s')",
                                           array($this->campaignID,$this->companyID,$this->globalGroupRule,date('Y-m-d H:i:s'),date('Y-m-d H:i:s')) ) );
      }

    }

  }

  /**
  ** Logs from gamesparks api
  **/
  private function Logs($request)
  {
    global $wpdb;

    $wpdb->query( "INSERT INTO Gamespark_logs (name,log,date) VALUES ('dropt','{$request}',NOW())" );

  }
}
