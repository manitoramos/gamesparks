<?php

ini_set( 'display_errors', 1 );

//INCLUDE WP-LOAD
require_once __DIR__ . '/../../../../wp-load.php';
require_once 'UploadImage.php';

new CreateDroptWP();

class CreateDroptWP
{

    function __construct ()
    {
        $this->GetData();
    }

    private function GetData ()
    {

        global $wpdb;

        $res = $wpdb->get_results( "SELECT * FROM Groups ORDER BY groupID ASC" );

        if ( sizeof( $res ) > 1 ) {
            for ($i = 0; $i < sizeof( $res ); $i++) {

                //loop on subgroup_ids
                $subids = explode( ",", $res[$i]->subGroupIDs );
                foreach ($subids as $id) {
                    if( !isset($id) || $id == "" ) break;

                    $subgroup = $wpdb->get_results( "SELECT * FROM SubGroups WHERE subGroupID = {$id}" );
                    if( $subgroup ) {
                        $post_id = wp_insert_post( [
                            'post_title' => $subgroup[0]->subGroupName,
                            'post_type' => "dropt",
                            'post_author' => 6,
                            "post_status" => 'publish'
                        ] );
                        update_post_meta( $post_id, "groupName", $res[$i]->groupName );
                        update_post_meta( $post_id, "company_dropt", $this->GetCompanyID( $subgroup[0]->sponsorCompanyID ) );
                        update_post_meta( $post_id, 'campaign_dropt', $this->GetCampaignID( $res[$i]->campaignID ) );
                        update_post_meta( $post_id, 'redemptionLocationIDs_dropt', $this->GetRedemptionsIDs( $subgroup[0]->redemptionLocationIDs ) );
                        update_post_meta( $post_id, 'termsAndConditions', $subgroup[0]->termsAndConditions );
                        update_post_meta( $post_id, 'rules', explode( ",", $res[$i]->groupRules ) );
                        update_post_meta( $post_id, 'prize', explode( ",", $subgroup[0]->subGroupRules ) );
                        update_post_meta( $post_id, 'qualify_count', $res[$i]->qualifyCount );
                        update_post_meta( $post_id, 'visibleUserEmails', $subgroup[0]->visibleUserEmails );
                        update_post_meta( $post_id, 'dateTimeCaptureBegin', date( "Y-m-d h:m:i", substr( $subgroup[0]->dateTimeCaptureBegin, 0, -3 ) ) );
                        update_post_meta( $post_id, 'dateTimeCaptureEnd', date( "Y-m-d h:m:i", substr( $subgroup[0]->dateTimeCaptureEnd, 0, -3 ) ) );
                        update_post_meta( $post_id, 'dateTimeRedeemBegin', date( "Y-m-d h:m:i", substr( $subgroup[0]->dateTimeRedeemBegin, 0, -3 ) ) );
                        update_post_meta( $post_id, 'dateTimeRedeemEnd', date( "Y-m-d h:m:i", substr( $subgroup[0]->dateTimeRedeemEnd, 0, -3 ) ) );
                        update_post_meta( $post_id, 'geoLocation_dropt', $this->GetGeoLocation( $subgroup[0]->geoLocationID ) );
                        update_post_meta( $post_id, 'radiusInMiles', $subgroup[0]->radiusInMiles );
                        update_post_meta( $post_id, 'duplicateThisMany', sizeof( explode( ',', $subgroup[0]->droptIDs ) ) );
                        update_post_meta( $post_id, 'offerCodes', $this->GetOfferCode( $subgroup[0]->droptIDs ) );
                        update_field( 'droptImagesURLs_0_parachutePayload', UploadImage( $subgroup[0]->parachutePayload, $post_id ), $post_id );
                        update_field( 'droptImagesURLs_0_parachuteOffer', UploadImage( $subgroup[0]->parachuteOffer, $post_id ), $post_id );
                        update_field( 'droptImagesURLs_0_capturedSuccess', UploadImage( $subgroup[0]->capturedSuccess, $post_id ), $post_id );
                        update_field( 'droptImagesURLs_0_whatsDroppinOffer', UploadImage( $subgroup[0]->whatsDroppinOffer, $post_id ), $post_id );
                        update_field( 'droptImagesURLs_0_walletIcon', UploadImage( $subgroup[0]->walletIcon, $post_id ), $post_id );
                        update_field( 'droptImagesURLs_0_contestHeaderOffer', UploadImage( $res[$i]->contestHeaderOffer, $post_id ), $post_id );
                        update_field( 'droptImagesURLs_0_contestHeaderIcon', UploadImage( $res[$i]->contestHeaderIcon, $post_id ), $post_id );
                        update_field( 'droptImagesURLs_0_contestProgressIcon', UploadImage( $subgroup[0]->contestProgressIcon, $post_id ), $post_id );
                        //to get the offercode image url is inside the dropt
                        //exit();
                    }
                }

            }
        }

    }

    /**
     * @param $company_id
     * @return array|object|string|null
     */
    private function GetCompanyID ( $company_id )
    {
        global $wpdb;

        $company_id = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key = 'company_id' AND meta_value = {$company_id}" );
        $company_id = isset( $company_id[0]->post_id ) ? $company_id[0]->post_id : "";

        return $company_id;
    }

    /**
     * @param $campaign_id
     * @return array|object|string|null
     */
    private function GetCampaignID ( $campaign_id )
    {
        global $wpdb;

        $campaign_id = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key = 'campaign_id' AND meta_value = {$campaign_id}" );
        $campaign_id = isset( $campaign_id[0]->post_id ) ? $campaign_id[0]->post_id : "";

        return $campaign_id;
    }

    /**
     * @param $redemptions_ids
     * @return array
     */
    private function GetRedemptionsIDs ( $redemptions_ids )
    {
        global $wpdb;
        $posts_ids = [];
        $redemptions_ids = explode(",", $redemptions_ids );

        foreach ( $redemptions_ids as $id ){
            $id = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key = 'redemption_id' AND meta_value = {$id}" );
            isset( $id[0]->post_id ) ? $posts_ids[] = $id[0]->post_id : "";
        }

        return $posts_ids;
    }

    /**
     * @param $geolocation_id
     * @return array|object|string|null
     */
    private function GetGeoLocation ( $geolocation_id )
    {
        global $wpdb;

        $geolocation_id = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key = 'geolocation_id' AND meta_value = {$geolocation_id}" );
        $geolocation_id = isset( $geolocation_id[0]->post_id ) ? $geolocation_id[0]->post_id : "";

        return $geolocation_id;
    }

    private function GetOfferCode( $dropt_ids )
    {
        global $wpdb;
        $dropt_ids = explode( ",", $dropt_ids );

        foreach ( $dropt_ids as $id ){
            $dropt = $wpdb->get_results( "SELECT * FROM Dropt WHERE droptID = {$id}" );
            if( sizeof($dropt) > 0 ){
                if( $dropt[0]->offerCodeImageURL != "" ){
                    return $dropt[0]->offerCodeImageURL;
                }
            }
        }
        return "";
    }

}
