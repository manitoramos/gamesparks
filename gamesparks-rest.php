<?php
/**
 * Plugin Name: _gamesparkes
 * Description: WP plugin developed by mobinteg.com
 * Author: mobinteg.com
 * Author URI: https://mobinteg.com
 * Version: 1.0.0
 */

ini_set('display_errors', 1);

//INCLUDE WP-LOAD
require_once __DIR__ . '/../../../wp-load.php';

//AUTH
require_once('services/Authentication.php');

//Custom Fields
require_once("services/CustomFields.php");
require_once("services/ErrorField.php");

//ROUTES
require_once('routes/Funcs.php');

//class
require_once("class/Controller.php");
require_once("class/Company.php");
require_once("class/Campaign.php");
require_once("class/GeoLocation.php");
require_once("class/RedemptionLocation.php");
require_once("class/Dropt.php");
require_once( "class/TriggerPrize.php" );

//didnt have trash option when value is 0
define('EMPTY_TRASH_DAYS', 0);
//define( 'AUTOSAVE_INTERVAL', 3600 );

  // $query = new WP_Query( array(
  //     'post_type'     => "dropt",
  //     'post_status'   => 'publish',
  //     'orderby'       => 'post_date'
  // ) );

//functions
// require_once('services/data_funcs.php');
// require_once('services/GeoLocation.php');
// require_once('services/RedemptionLocation.php');
// require_once('services/Campaign.php');
// require_once('services/Dropt.php');
