<?php

// ini_set( 'display_errors', 1 );
//
// //INCLUDE WP-LOAD
// require_once __DIR__ . '/../../../../wp-load.php';
//
// require_once 'config.php';
//
// new ImportRedemption();

class ImportRedemption
{
    private $token;

    //url preview or live
    private $prodbd;

    function __construct ()
    {
        $this->prodbd = "live";
        $this->AuthenticationProd();
        $this->Data();
        //$this->GetData( "companyID", "CAMPAIGN" );
    }

    /**
     * @param $field
     */
    private function GetData ($field, $tablename)
    {
        $params = '{
        "asc": true,
        "query":
            {
            "field": "' . $field . '",
            "operator": "NE",
            "value": "-1"
            }
        }';

        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, 'https://' . API_KEY . ".{$this->prodbd}.cluster.gamesparks.net/restv2/game/" . API_KEY . '/data/' . $tablename . '/query' );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $params );
        curl_setopt( $ch, CURLOPT_POST, 1 );

        $headers = array();
        $headers[] = 'Content-Type: application/json;charset=UTF-8';
        $headers[] = 'Accept: application/json';
        $headers[] = 'X-Gs-Jwt: ' . $this->token . '';
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

        $result = curl_exec( $ch );
        if ( curl_errno( $ch ) ) {
            echo 'Error:' . curl_error( $ch );
        }
        curl_close( $ch );
        $result = json_decode( $result );
        if ( isset( $result->errorCode ) && $result->errorCode == "token_expired" ) {
            //get new token and repeat
            echo $result;
        } else if ( isset( $result->errorCode ) && $result->errorCode != "token_expired" ) {
            //other error
            $this->token;
            echo json_encode( $result );
        } else if ( !isset( $result->errorCode ) ) {
            return $result;
            //echo json_encode( $result );
        }
        //exit();
    }

    /**
     * @param $data
     */
    private function Data ()
    {

        //data from tables gamesparks
        $links = $this->GetData( "locationName", "REDEMPTIONlinks" );
        $status = $this->GetData( "latitude", "REDEMPTIONstatus" );
        $address = $this->GetData( "city", "REDEMPTIONaddress" );
        //create if not exists
        global $wpdb;
        for ($i = 0; $i < sizeof( $links->results ); $i++) {
            if ( $links->results[$i]->id != "NaN" ) {
                $res = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key = 'redemption_id' AND meta_value = {$links->results[$i]->id} " );
                if ( sizeof( $res ) == 0 ) {

                    for ($e = 0; $e < sizeof( $status->results ); $e++) {
                        //loop until get the same id on status
                        if ( $status->results[$e]->id == $links->results[$i]->id ) {
                            //loop until get the same id on address
                            for ($f = 0; $f < sizeof( $address->results ); $f++) {
                                if ( $address->results[$f]->id == $links->results[$i]->id ) {
                                    $this->CreateRedemption( $links->results[$i], $address->results[$f]->data, $status->results[$e]->data );
                                    break;
                                }
                            }
                            break;
                        }
                    }

                }
            }
        }
    }

    /**
     * @param $links
     * @param $address
     * @param $status
     */
    private function CreateRedemption ($links, $address, $status)
    {
        global $wpdb;

        //CREATE ON WORDPRESS
        $post_id = wp_insert_post( [
            'post_title' => $links->data->locationName,
            'post_type' => "redemptionlocation",
            'post_author' => 6,
            "post_status" => 'publish'
        ] );

        $company_id = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key = 'company_id' AND meta_value = {$links->data->companyID}" );
        $company_postid = isset( $company_id[0]->post_id ) ? $company_id[0]->post_id : "";//wordpress
        $company_id = isset( $company_id[0]->meta_value ) ? $company_id[0]->meta_value : "";//our table

        $geolocation_id = $wpdb->get_results( "SELECT * FROM GeoLocation
                                                     WHERE latitude = '{$status->latitude}' AND longitude = '{$status->longitude}'" );
        if ( isset( $geolocation_id[0]->geoLocationID ) ) {
            $geolocation_id = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key = 'geolocation_id' AND meta_value = {$geolocation_id[0]->geoLocationID}" );
            $geolocation_postid = isset( $geolocation_id[0]->post_id ) ? $geolocation_id[0]->post_id : "";//wordpress
            $geolocation_id = isset( $geolocation_id[0]->meta_value ) ? $geolocation_id[0]->meta_value : "";//our table
        }

        update_post_meta( $post_id, 'redemption_id', $links->id );
        update_post_meta( $post_id, 'street1', $address->street1 );
        update_post_meta( $post_id, 'street2', $address->street2 );
        update_post_meta( $post_id, 'city', $address->city );
        update_post_meta( $post_id, 'zipCode', $address->zipCode );
        update_post_meta( $post_id, 'state', $address->state );
        update_post_meta( $post_id, 'radiusInMiles', $status->radiusInMiles );
        update_post_meta( $post_id, 'phonenumber', $links->data->phoneNumber );
        update_post_meta( $post_id, 'company_redemption_location', $company_postid );
        update_post_meta( $post_id, 'geoLocation_redemption_location', isset( $geolocation_postid ) ? $geolocation_postid : "" );

        //CREATE ON OUR TABLES
        $created = isset( $links->data->dateTimeCreated ) ? date( 'Y-m-d H:i:s', time( $links->data->dateTimeCreated ) ) : "";
        $modified = date( 'Y-m-d H:i:s', time( $links->data->dateTimeModified ) );

        if ( gettype( $geolocation_id ) == "array" ) $geolocation_id = "";

        $wpdb->query(
            $wpdb->prepare( "INSERT INTO RedemptionLocation
                (redemptionID,companyID,locationName,phoneNumber,street1,street2,city,
                state,zipCode,radiusinMiles,geoLocationID,dateTimeCreated,dateTimeModified)
                VALUES ('%d','%d','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                array($links->id, $company_id, $links->data->locationName, $links->data->phoneNumber, $address->street1, $address->street2,
                    $address->city, $address->state, $address->zipCode, $status->radiusInMiles, $geolocation_id,
                    $created, $modified) ) );
    }

    /**
     * @return string
     */
    private function Authentication ()
    {
        // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, "https://auth.gamesparks.net/restv2/auth/game/" . API_KEY . "/jwt" );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );


        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: Basic dmRlbGdhZG9AbW9iaW50ZWcuY29tOnF1ZWlqbzEyMyE=';
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

        $result = curl_exec( $ch );
        if ( curl_errno( $ch ) ) {
            echo 'Error:' . curl_error( $ch );
        }
        curl_close( $ch );
        //echo $result;
        $result = (array)json_decode( $result );
        //token
        if ( isset( $result["X-GS-JWT"] ) ) {
            $this->token = $result["X-GS-JWT"];
        } else {
            return "error";
        }
    }

    private function AuthenticationProd ()
    {
        // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, "https://auth.gamesparks.net/restv2/auth/game/" . API_KEY . "/jwt" );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );


        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: Basic amFzb24ua2VsbGV5QGRyb3B0aW5jLmNvbTpXb3JsZHJhY2UxMjMj';
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

        $result = curl_exec( $ch );
        if ( curl_errno( $ch ) ) {
            echo 'Error:' . curl_error( $ch );
        }
        curl_close( $ch );
        //echo $result;
        $result = (array)json_decode( $result );
        //token
        if ( isset( $result["X-GS-JWT"] ) ) {
            $this->token = $result["X-GS-JWT"];
        } else {
            return "error";
        }
    }


}
