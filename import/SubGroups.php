<?php

// ini_set( 'display_errors', 1 );
//
// //INCLUDE WP-LOAD
// require_once __DIR__ . '/../../../../wp-load.php';
//
// require_once 'config.php';
//
// new ImportGroups();

class ImportSubGroups
{
    private $token;

    function __construct ()
    {
        $this->AuthenticationProd();
        $this->Data();
        //$this->GetData( "groupName", "GROUP" );
    }

    /**
     * @param $field
     * @param $tablename
     * @return array|bool|mixed|object|string|null
     */
    private function GetData ($field, $tablename)
    {
        $params = '{
        "asc": true,
        "query":
            {
            "field": "' . $field . '",
            "operator": "NE",
            "value": "-1"
            }
        }';

        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, 'https://' . API_KEY . '.preview.cluster.gamesparks.net/restv2/game/' . API_KEY . '/data/'. $tablename .'/query' );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $params );
        curl_setopt( $ch, CURLOPT_POST, 1 );

        $headers = array();
        $headers[] = 'Content-Type: application/json;charset=UTF-8';
        $headers[] = 'Accept: application/json';
        $headers[] = 'X-Gs-Jwt: ' . $this->token . '';
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

        $result = curl_exec( $ch );
        if ( curl_errno( $ch ) ) {
            echo 'Error:' . curl_error( $ch );
        }
        curl_close( $ch );
        $result = json_decode( $result );
        if ( isset( $result->errorCode ) && $result->errorCode == "token_expired" ) {
            //get new token and repeat
            echo $result;
        } else if ( isset( $result->errorCode ) && $result->errorCode != "token_expired" ) {
            //other error
            $this->token;
            echo json_encode( $result );
        } else if ( !isset( $result->errorCode ) ) {
            return $result;
            //echo json_encode( $result );
        }
        //exit();
    }

    /**
     *
     */
    private function Data ( )
    {
        //data from tables gamesparks
        $info   = $this->GetData( "subGroupName", "SUBGROUPinfo" );
        $links  = $this->GetData( "droptIDs", "SUBGROUPlinks" );
        $times  = $this->GetData( 'dateTimeRedeemBegin', 'SUBGROUPtimes' );
        $status = $this->GetData( 'isPrize', 'SUBGROUPstatus' );
        //create if not exists
        global $wpdb;
        //****************************************************
        // i = info :: e = status :: f = $links :: g = times *
        //****************************************************
        for ($i = 0; $i < sizeof( $info->results ); $i++) {
            if ( $info->results[$i]->id != "NaN" ) {
                $res = $wpdb->get_results( "SELECT * FROM SubGroups WHERE subGroupID = {$info->results[$i]->id} " );
                if ( sizeof( $res ) == 0 ) {

                    for ($e = 0; $e < sizeof( $status->results ); $e++) {
                        //loop until get the same id on status
                        if ( $status->results[$e]->id == $info->results[$i]->id ) {
                            //loop until get the same id on links
                            for ($f = 0; $f < sizeof( $links->results ); $f++) {
                                if ( $links->results[$f]->id == $info->results[$i]->id ) {
                                    //loop until get the same id on times
                                    for ($g = 0; $g < sizeof( $times->results ); $g++) {
                                        if ( $times->results[$g]->id == $info->results[$i]->id ) {
                                            $this->CreateSubGroup( $info->results[$i], $links->results[$f]->data, $times->results[$g]->data, $status->results[$e]->data );
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                    }

                }
            }
        }
    }

    /**
     * @param $info
     * @param $links
     * @param $times
     * @param $status
     */
    private function CreateSubGroup ( $info, $links, $times, $status )
    {

        global $wpdb;

        try {
            //CREATE ON OUR TABLES
            $created  = isset( $links->dateTimeCreated ) ? $links->dateTimeCreated : "";
            $modified = $status->dateTimeModified ;
            $wpdb->query(
                $wpdb->prepare("INSERT INTO SubGroups
                (subGroupID,subGroupName,heldUserIDs,visibleUserEmails,dailyLimit,subGroupRules,termsAndConditions,contestProgressIcon,
                    dateTimeCreated,walletIcon,whatsDroppinOffer,droptIDs,capturedSuccess,groupID,parachuteOffer,parachutePayload,
                    redemptionLocationIDs,sponsorCompanyID,dateTimeRedeemBegin,dateTimeCaptureBegin,dateTimeRedeemEnd,dateTimeCaptureEnd,
                    isPrize,dateTimeModified,radiusInMiles,redeemTimeStatus,captureTimeStatus,isRegularDropt,geoLocationID)
                VALUES ('%d','%s','%s','%s','%d','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',
                    '%s','%s','%s','%d','%s','%s','%s','%s','%d','%s')",
                    array( $info->id, $info->data->subGroupName, $info->data->heldUserIDs, $info->data->visibleUserEmails,
                        $info->data->dailyLimit, $info->data->subGroupRules, $info->data->termsAndConditions,
                        $links->contestProgressIcon, $created, $links->walletIcon, $links->whatsDroppinOffer, $links->droptIDs,
                        $links->capturedSuccess, $links->groupID, $links->parachuteOffer, $links->parachutePayload,
                        $links->redemptionLocationIDs, $links->sponsorCompanyID, $times->dateTimeRedeemBegin,
                        $times->dateTimeCaptureBegin, $times->dateTimeRedeemEnd, $times->dateTimeCaptureEnd,
                        $status->isPrize, $modified, $status->radiusInMiles, $status->redeemTimeStatus, $status->captureTimeStatus,
                        $status->isRegularDropt, $status->geoLocationID ) ) );
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    /**
     * @return string
     */
    private function Authentication ()
    {
        // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, "https://auth.gamesparks.net/restv2/auth/game/" . API_KEY . "/jwt" );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );


        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: Basic dmRlbGdhZG9AbW9iaW50ZWcuY29tOnF1ZWlqbzEyMyE=';
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

        $result = curl_exec( $ch );
        if ( curl_errno( $ch ) ) {
            echo 'Error:' . curl_error( $ch );
        }
        curl_close( $ch );
        //echo $result;
        $result = (array)json_decode( $result );
        //token
        if ( isset( $result["X-GS-JWT"] ) ) {
            $this->token = $result["X-GS-JWT"];
        } else {
            return "error";
        }
    }

    private function AuthenticationProd ()
    {
        // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, "https://auth.gamesparks.net/restv2/auth/game/" . API_KEY . "/jwt" );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );


        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: Basic amFzb24ua2VsbGV5QGRyb3B0aW5jLmNvbTpXb3JsZHJhY2UxMjMj';
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

        $result = curl_exec( $ch );
        if ( curl_errno( $ch ) ) {
            echo 'Error:' . curl_error( $ch );
        }
        curl_close( $ch );
        //echo $result;
        $result = (array)json_decode( $result );
        //token
        if ( isset( $result["X-GS-JWT"] ) ) {
            $this->token = $result["X-GS-JWT"];
        } else {
            return "error";
        }
    }


}
