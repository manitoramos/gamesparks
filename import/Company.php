<?php

// ini_set( 'display_errors', 1 );
//
// //INCLUDE WP-LOAD
// require_once __DIR__ . '/../../../../wp-load.php';
//
// require_once 'config.php';
//
// new ImportCompanies();

class ImportCompanies
{
    private $token;

    //url preview or live
    private $prodbd;

    function __construct ()
    {
        $this->prodbd = "live";
        $this->AuthenticationProd();
        $this->Data();
        //$this->GetData( "campaignIDs", "COMPANYlinks" );
    }

    /**
     * @param $field
     * @param $tablename
     * @return array|bool|mixed|object|string|null
     */
    private function GetData ($field, $tablename)
    {
        $params = '{
        "asc": true,
        "query":
            {
            "field": "' . $field . '",
            "operator": "NE",
            "value": "-1"
            }
        }';

        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, 'https://' . API_KEY . ".{$this->prodbd}.cluster.gamesparks.net/restv2/game/" . API_KEY . '/data/'. $tablename .'/query' );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $params );
        curl_setopt( $ch, CURLOPT_POST, 1 );

        $headers = array();
        $headers[] = 'Content-Type: application/json;charset=UTF-8';
        $headers[] = 'Accept: application/json';
        $headers[] = 'X-Gs-Jwt: ' . $this->token . '';
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

        $result = curl_exec( $ch );
        if ( curl_errno( $ch ) ) {
            echo 'Error:' . curl_error( $ch );
        }
        curl_close( $ch );
        $result = json_decode( $result );
        if ( isset( $result->errorCode ) && $result->errorCode == "token_expired" ) {
            //get new token and repeat
            echo $result;
        } else if ( isset( $result->errorCode ) && $result->errorCode != "token_expired" ) {
            //other error
            $this->token;
            echo json_encode( $result );
        } else if ( !isset( $result->errorCode ) ) {
            return $result;
            //echo json_encode( $result );
        }
        //exit();
    }

    /**
     *
     */
    private function Data ( )
    {

        //data from tables gamesparks
        $companylinks = $this->GetData( "campaignIDs", 'COMPANYlinks' );
        $companyaddress= $this->GetData( 'city', 'COMPANYaddress' );
        //create if not exists
        global $wpdb;
        for( $i = 0; $i < sizeof( $companylinks->results ); $i++ ){
            $res = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key = 'company_id' AND meta_value = {$companylinks->results[$i]->id} " );
            if( sizeof($res) == 0 ){
                for( $e = 0; $e < sizeof( $companyaddress->results ); $e++ ){
                    //loop until get the same id on links
                    if( $companyaddress->results[$e]->id == $companylinks->results[$i]->id ){

                        $this->CreateCompany( $companylinks->results[$i], $companyaddress->results[$e]->data);
                        break;
                    }
                }
            }
        }
    }

    /**
     * @param $datalinks
     * @param $data_address
     */
    private function CreateCompany ( $datalinks, $data_address )
    {
        global $wpdb;

        //CREATE ON WORDPRESS
        $post_id = wp_insert_post( [
            'post_title' => $datalinks->data->companyName,
            'post_type'  => "company",
            'post_author' => 6,
            "post_status"=> 'publish'
        ] );
        update_post_meta( $post_id, "phoneNumber", $datalinks->data->phoneNumber );
        update_post_meta( $post_id, "city", $data_address->city );
        update_post_meta( $post_id, 'street1', $data_address->street1 );
        update_post_meta( $post_id, 'street2', $data_address->street2 );
        update_post_meta( $post_id, 'state', $data_address->state );
        update_post_meta( $post_id, 'zipCode', $data_address->zipCode );
        update_post_meta( $post_id, 'company_id', $datalinks->id );

        //echo date( 'Y-m-d H:i:s', time( 1549032849957 ) );
        //CREATE ON OUR TABLES
        $created  = isset( $datalinks->data->dateTimeCreated ) ? date( 'Y-m-d H:i:s', time( $datalinks->data->dateTimeCreated ) ) : "";
        $modified = date( 'Y-m-d H:i:s', time( $datalinks->data->dateTimeModified ) ) ;
        $wpdb->query(
            $wpdb->prepare("INSERT INTO Company
                (companyID,campaignIDs,companyName,phoneNumber,street1,street2,city,state,zipCode, dateTimeCreated, dateTimeModified)
                VALUES ('%d','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                array($datalinks->id, $datalinks->data->campaignIDs , $datalinks->data->companyName, $datalinks->data->phoneNumber,$data_address->street1,
                     $data_address->street2, $data_address->city, $data_address->state, $data_address->zipCode, $created, $modified ) ) );
    }

    /**
     * @return string
     */
    private function Authentication ()
    {
        // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, "https://auth.gamesparks.net/restv2/auth/game/" . API_KEY . "/jwt" );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );


        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: Basic dmRlbGdhZG9AbW9iaW50ZWcuY29tOnF1ZWlqbzEyMyE=';
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

        $result = curl_exec( $ch );
        if ( curl_errno( $ch ) ) {
            echo 'Error:' . curl_error( $ch );
        }
        curl_close( $ch );
        //echo $result;
        $result = (array)json_decode( $result );
        //token
        if ( isset( $result["X-GS-JWT"] ) ) {
            $this->token = $result["X-GS-JWT"];
        } else {
            return "error";
        }
    }

    private function AuthenticationProd ()
    {
        // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, "https://auth.gamesparks.net/restv2/auth/game/" . API_KEY . "/jwt" );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );


        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: Basic amFzb24ua2VsbGV5QGRyb3B0aW5jLmNvbTpXb3JsZHJhY2UxMjMj';
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

        $result = curl_exec( $ch );
        if ( curl_errno( $ch ) ) {
            echo 'Error:' . curl_error( $ch );
        }
        curl_close( $ch );
        //echo $result;
        $result = (array)json_decode( $result );
        //token
        if ( isset( $result["X-GS-JWT"] ) ) {
            $this->token = $result["X-GS-JWT"];
        } else {
            return "error";
        }
    }


}

?>
