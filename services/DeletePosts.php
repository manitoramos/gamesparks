<?php
/**
 * Created by PhpStorm.
 * User: tiago
 * Date: 2/14/19
 * Time: 2:28 PM
 */

//INCLUDE WP-LOAD
require_once __DIR__ . '/../../../../wp-load.php';

function DeletePosts()
{
    global $wpdb;

    $postsnames = [
        "company",
        "campaign",
        "geolocation",
        "redemptionlocation",
        "dropt",
    ];

    foreach ( $postsnames as $postname ){
        $args = array( 'posts_per_page' => -1, 'post_type' => $postname, );
        $allposts= get_posts( $args );
        foreach ( $allposts as $post ){
            wp_delete_post( $post->ID, true );
        }
    }


    $wpdb->query( "TRUNCATE TABLE Company" );
    $wpdb->query( "TRUNCATE TABLE Campaign" );
    $wpdb->query( "TRUNCATE TABLE GeoLocation" );
    $wpdb->query( "TRUNCATE TABLE RedemptionLocation" );
    $wpdb->query( "TRUNCATE TABLE Groups" );
    $wpdb->query( "TRUNCATE TABLE SubGroups" );



}

DeletePosts();
