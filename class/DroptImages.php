<?php


/**
 *
 */
class DroptImages extends Dropt
{
  /**
  ** meta_key
  **/
  private function GetImage( string $meta_key )
  {
    global $wpdb;
    global $post;

    $res = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta WHERE post_id = {$post->ID} AND meta_key = '{$meta_key}'" );
    //post id image
    $value = $res[0]->meta_value;
    if(!isset($value) || $value === "" || $value === null ){
      return "";
    }

    $res = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta WHERE post_id = {$value} AND meta_key = 'redemption_id'" );
    //image url
    return $res[0]->meta_value;
  }


}
