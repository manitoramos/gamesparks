<?php

// ini_set( 'display_errors', 1 );
//
// //INCLUDE WP-LOAD
// require_once __DIR__ . '/../../../../wp-load.php';
//
// require_once 'config.php';
//
// new ImportCampaigns();

class ImportCampaigns
{
    private $token;
    private $custom_value = "";
    private $custom_name = "";

    //url preview or live
    private $prodbd;

    function __construct ()
    {
        $this->prodbd = "live";
        $this->AuthenticationProd();
        $this->Data();
        //$this->GetData( "companyID", "CAMPAIGN" );
    }

    /**
     * @param $field
     */
    private function GetData ($field, $tablename)
    {
        $params = '{
        "asc": true,
        "query":
            {
            "field": "' . $field . '",
            "operator": "NE",
            "value": "-1"
            }
        }';

        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, 'https://' . API_KEY . ".{$this->prodbd}.cluster.gamesparks.net/restv2/game/" . API_KEY . '/data/' . $tablename . '/query' );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $params );
        curl_setopt( $ch, CURLOPT_POST, 1 );

        $headers = array();
        $headers[] = 'Content-Type: application/json;charset=UTF-8';
        $headers[] = 'Accept: application/json';
        $headers[] = 'X-Gs-Jwt: ' . $this->token . '';
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

        $result = curl_exec( $ch );
        if ( curl_errno( $ch ) ) {
            echo 'Error:' . curl_error( $ch );
        }
        curl_close( $ch );
        $result = json_decode( $result );
        if ( isset( $result->errorCode ) && $result->errorCode == "token_expired" ) {
            //get new token and repeat
            echo $result;
        } else if ( isset( $result->errorCode ) && $result->errorCode != "token_expired" ) {
            //other error
            $this->token;
            echo json_encode( $result );
        } else if ( !isset( $result->errorCode ) ) {
            return $result;
            //echo json_encode( $result );
        }
        //exit();
    }

    /**
     * @param $data
     */
    private function Data ()
    {

        //data from tables gamesparks
        $campaign = $this->GetData( "companyID", "CAMPAIGN" );
        //create if not exists
        global $wpdb;
        for ($i = 0; $i < sizeof( $campaign->results ); $i++) {
            $res = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key = 'campaign_id' AND meta_value = {$campaign->results[$i]->id} " );
            if ( sizeof( $res ) == 0 ) {
                $this->CreateCampaign( $campaign->results[$i] );
            }
        }
    }

    /**
     * @param $campaign
     */
    private function CreateCampaign ($campaign)
    {
        global $wpdb;

        //CREATE ON WORDPRESS
        $post_id = wp_insert_post( [
            'post_title' => "Campaign Nº" . $campaign->id,
            'post_type' => "campaign",
            'post_author' => 6,
            "post_status" => 'publish'
        ] );

        $company_id = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key = 'company_id' AND meta_value = {$campaign->data->companyID}" );
        $company_id = isset( $company_id[0]->post_id ) ? $company_id[0]->post_id : "";

        update_post_meta( $post_id, "company_campaign", $company_id );
        update_post_meta( $post_id, 'campaign_id', $campaign->id );
        //get the custom values if they exists
        $this->Custom($campaign);
        //check the group rule
        if ( $campaign->data->globalGroupRule == "eachGroup" ) {
            update_post_meta( $post_id, 'numQualifies', $campaign->data->numQualifies );
            update_post_meta( $post_id, "globalGroupRule", "each_group_" );
        } else if ( $campaign->data->globalGroupRule == "allGroups" ) {
            update_post_meta( $post_id, 'numQualifies', $campaign->data->numQualifies );
            update_post_meta( $post_id, 'numgroups', $campaign->data->numGroups );
            update_post_meta( $post_id, "globalGroupRule", "each_group_" );
        } else if ( $campaign->data->globalGroupRule == "custom" ) {
            update_post_meta( $post_id, 'custom_name', $this->custom_name );
            update_post_meta( $post_id, 'custom_value', $this->custom_value );
            update_post_meta( $post_id, "globalGroupRule", "custom" );
        }


        //echo date( 'Y-m-d H:i:s', time( 1549032849957 ) );
        //CREATE ON OUR TABLES
        $created = isset( $campaign->data->dateTimeCreated ) ? date( 'Y-m-d H:i:s', time( $campaign->data->dateTimeCreated ) ) : "";
        $modified = date( 'Y-m-d H:i:s', time( $campaign->data->dateTimeModified ) );
        $qualifies = isset( $campaign->data->numQualifies ) ? $campaign->data->numQualifies : "";
        $numgroups = isset( $campaign->data->numGroups ) ? $campaign->data->numGroups : "";

        $wpdb->query(
            $wpdb->prepare( "INSERT INTO Campaign
                (campaignID,companyID,globalGroupRule,numQualifies,numGroups,custom_name,custom_value,dateTimeCreated,dateTimeModified)
                VALUES ('%d','%d','%s','%s','%s','%s','%s','%s','%s')",
                array($campaign->id, $company_id, $campaign->data->globalGroupRule, $qualifies,
                    $numgroups, $this->custom_name, $this->custom_value, $created, $modified) ) );
    }

    /**
     * @return string
     */
    private function Authentication ()
    {
        // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, "https://auth.gamesparks.net/restv2/auth/game/" . API_KEY . "/jwt" );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );


        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: Basic dmRlbGdhZG9AbW9iaW50ZWcuY29tOnF1ZWlqbzEyMyE=';
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

        $result = curl_exec( $ch );
        if ( curl_errno( $ch ) ) {
            echo 'Error:' . curl_error( $ch );
        }
        curl_close( $ch );
        //echo $result;
        $result = (array)json_decode( $result );
        //token
        if ( isset( $result["X-GS-JWT"] ) ) {
            $this->token = $result["X-GS-JWT"];
        } else {
            return "error";
        }
    }

    /**
     * @param $campaign
     * populate the customs
     */
    private function Custom ($campaign)
    {

        if ( isset( $campaign->data->regularDroptGroupName ) ) {
            $this->custom_name = 'regularDroptGroupName';
            $this->custom_value = $campaign->data->regularDroptGroupName;
        } else if ( isset( $campaign->data->contestGroupName ) ) {
            $this->custom_name = 'contestGroupName';
            $this->custom_value = $campaign->data->contestGroupName;
        } else if ( isset( $campaign->data->scavengerHuntGroupName ) ) {
            $this->custom_name = 'scavengerHuntGroupName';
            $this->custom_value = $campaign->data->scavengerHuntGroupName;
        }

    }

    private function AuthenticationProd ()
    {
        // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, "https://auth.gamesparks.net/restv2/auth/game/" . API_KEY . "/jwt" );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );


        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: Basic amFzb24ua2VsbGV5QGRyb3B0aW5jLmNvbTpXb3JsZHJhY2UxMjMj';
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

        $result = curl_exec( $ch );
        if ( curl_errno( $ch ) ) {
            echo 'Error:' . curl_error( $ch );
        }
        curl_close( $ch );
        //echo $result;
        $result = (array)json_decode( $result );
        //token
        if ( isset( $result["X-GS-JWT"] ) ) {
            $this->token = $result["X-GS-JWT"];
        } else {
            return "error";
        }
    }


}
