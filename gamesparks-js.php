<?php
    $action = $_POST['action'];
    if( !$action ) exit();

    //echo $action;
    //exit();

    ini_set('display_errors', 1);
    //INCLUDE WP-LOAD
    require_once __DIR__ . '/../../../wp-load.php';
    //AUTH
    require_once('services/Authentication.php');

    //class
    require_once("javascript/Company.php");
    //require_once("class/Campaign.php");
    //require_once("class/GeoLocation.php");
    //require_once("class/RedemptionLocation.php");
    //require_once("class/Dropt.php");
    //require_once( "class/TriggerPrize.php" );

    //
    switch ($_POST['action']) {
        case 'company':
            new CompanyJS();
            break;
        case 'campaign':
            new Campaign();
            break;
        case 'geolocation':
            new GeoLocation();
            break;
        case 'redemptionlocation':
            new Redemption();
            break;
        case 'dropt':
            new Dropt();
            break;
        case 'prize':
            new TriggerPrize();
            break;
    }
