<?php

/**
 * GameSparks
 */
class GeoLocation extends ControllerSparks
{
    private $geoID;
    private $geoName;
    private $latitude;
    private $longitude;

    function __construct()
    {
        $this->Configs( "prod" );

        global $post,$wpdb;

        //get the title
        $title = $wpdb->get_results("select * from {$wpdb->prefix}posts where ID = {$post->ID}");
        $this->geoName = $title[0]->post_title;

        //values to vars
        $this->latitude = get_field('latitude');
        $this->longitude = get_field('longitude');

        if (get_field('geolocation_id') !== null && get_field('geolocation_id') !== "") {
            $this->geoID = get_field('geolocation_id');

            //update geolocation
            $this->UpdateGeoLocation();
        } else {//create
            update_post_meta($post->ID, 'geolocation_id', $this->CreateGeoLocation());
        }

    }

    /**
     **
     **/
    private function CreateGeoLocation()
    {
        global $post;

        $ch = curl_init();

        $params = '{
          "@class": ".LogEventRequest",
          "eventKey": "CreateGeoLocation",
          "geoLocationName": "' . $this->geoName . '",
          "latitude": ' . $this->latitude . ',
          "longitude": ' . $this->longitude . ',
          "playerId": "' . playerID . '"
        }';

        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        $headers[] = "Content-Type: application/json";
        $headers[] = "Accept: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        } else {
            $this->Logs($result);
            update_post_meta($post->ID, 'box_log', $result);
            update_post_meta( $post->ID, 'query_log', $params );
            $result = json_decode($result);
        }
        curl_close($ch);
        if ( isset($result->scriptData->new_geoLocation_ID) ) {
            $this->geoID = intval($result->scriptData->new_geoLocation_ID);
            $this->DataBase('insert');
            return $result->scriptData->new_geoLocation_ID;
        }else{
            $my_post = array(
                'ID'           => $post->ID,
                'post_status'   => 'error',
            );
            wp_update_post( $my_post );
        }
    }

    /**
     **
     **/
    private function UpdateGeoLocation()
    {
        global $post,$wpdb;

        try {
            $ch = curl_init();

            $params = '{
              "@class": ".LogEventRequest",
              "eventKey": "EditGeoLocation",
              "geoLocationID": "' . $this->geoID . '",
              "geoLocationName": "' . $this->geoName . '",
              "latitude": ' . floatval($this->latitude) . ',
              "longitude": ' . floatval($this->longitude) . ',
              "playerId": "' . playerID . '"
            }';

            curl_setopt($ch, CURLOPT_URL, $this->url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_POST, 1);

            $headers = array();
            $headers[] = "Content-Type: application/json";
            $headers[] = "Accept: application/json";
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
                $this->Logs(curl_error($ch));
            } else {
                $this->Logs($result);
                update_post_meta($post->ID, 'box_log', $result);
                update_post_meta( $post->ID, 'query_log', $params );
                $result = json_decode($result);
                //return isset($result->scriptData->update)
            }
            curl_close($ch);
            if( $result->scriptData->update == "success" ){
                $this->DataBase("update");
            } else{
                $res = $wpdb->get_results( "SELECT * FROM GeoLocation WHERE geoLocationID = $this->geoID" );
                //on error put the data to the same old value
                update_post_meta( $post->ID, 'geolocation_id', $this->geoID );
                update_post_meta( $post->ID, 'latitude', $res[0]->latitude );
                update_post_meta( $post->ID, 'longitude', $res[0]->longitude );
                wp_update_post( array('ID' => $post->ID, 'post_title' => "{$res[0]->geoLocationName}" ) );
            }
        } catch (Exception $e) {
            $this->Logs($e);
        }
    }

    /**
     **
     **/
    private function DataBase($task)
    {
        global $wpdb;

        if ($task == "insert") {
            $wpdb->query($wpdb->prepare("INSERT INTO GeoLocation (geoLocationID,geoLocationName,latitude,longitude,dateTimeCreated,dateTimeModified)
                                         VALUES ('%d','%s','%s','%s','%s','%s')",
                array($this->geoID, $this->geoName, $this->latitude, $this->longitude, date('Y-m-d H:i:s'), date('Y-m-d H:i:s'))));
        } else if ($task == "update") {
            //check if exists on database otherwise insert again
            $res = $wpdb->get_results($wpdb->prepare("SELECT * FROM GeoLocation WHERE geoLocationID = %d", array($this->geoID)));

            if (isset($res[0])) {
                $wpdb->get_results($wpdb->prepare("UPDATE GeoLocation
                                             SET geoLocationName = '%s', latitude = '%s', longitude = '%s', dateTimeModified = '%s'
                                             WHERE geoLocationID = %d", array($this->geoName, $this->latitude, $this->longitude, date('Y-m-d H:i:s'), $this->geoID)));
            } else {
                $wpdb->query($wpdb->prepare("INSERT INTO GeoLocation (geoLocationID,geoLocationName,latitude,longitude,dateTimeCreated,dateTimeModified)
                                           VALUES ('%d','%s','%s','%s','%s','%s')",
                    array($this->geoID, $this->geoName, $this->latitude, $this->longitude, date('Y-m-d H:i:s'), date('Y-m-d H:i:s'))));
            }

        }

    }

    /**
     ** Logs from gamesparks api
     **/
    private function Logs($request)
    {
        global $wpdb;

        $wpdb->query("INSERT INTO Gamespark_logs (name,log,date) VALUES ('geoLocation','{$request}',NOW())");

    }

}
