<?php

ini_set( 'display_errors', 1 );

//INCLUDE WP-LOAD
require_once __DIR__ . '/../../../wp-load.php';

//require_once 'import/config.php';
require_once 'import/configprodution.php';

require_once 'import/AuthProdution.php';

//require_once 'import/ProdutionBackUp.php';

require_once 'import/Campaign.php';
require_once 'import/Company.php';
require_once 'import/GeoLocation.php';
//require_once 'import/Dropt.php';
require_once 'import/Redemption.php';
//require_once 'import/SubGroups.php';
require_once 'import/Groups.php';

new ImportCompanies();
new ImportCampaigns();
new ImportGeoLocations();
new ImportRedemption();
new ImportGroups();
//new ImportSubGroups();
//new ImportDropts();
