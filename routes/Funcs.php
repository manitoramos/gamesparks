<?php


add_action('acf/save_post', 'my_acf_save_post', 20);


function my_acf_save_post($post_id)
{
    global $post;

    //on save draft click dont make requests to GameSparks
    if( isset( $post->ID) ) {
        if ( CheckPostStatus( $post->ID ) == "draft" ) return;
    }

    switch ($post->post_type) {
        case 'company':
            new Company();
            break;
        case 'campaign':
            new Campaign();
            break;
        case 'geolocation':
            new GeoLocation();
            break;
        case 'redemptionlocation':
            new Redemption();
            break;
        case 'dropt':
            new Dropt();
            break;
        case 'prize':
            new TriggerPrize();
            break;
    }

}

/**
 * Check the post_status
 */
function CheckPostStatus( int $post_id )
{
    global $wpdb;

    $res = $wpdb->get_results( "select * from {$wpdb->prefix}posts where ID = {$post_id}" );

    if($res) return $res[0]->post_status;
}
