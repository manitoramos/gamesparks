<?php

require_once("../config/config.php");
require_once("../services/Authentication.php");

//echo $_POST['companyName'];
//exit();

new CompanyCreate();


/**
 * GameSparks
 */
class CompanyCreate
{
  private $companyID;
  private $companyName;
  private $phoneNumber;
  private $street1;
  private $street2;
  private $city;
  private $state;
  private $zipCode;

  function __construct()
  {
    global $post;

    //values to vars
    $this->companyName = $_POST['companyName'];
    $this->phoneNumber = $_POST['phoneNumber'];
    $this->street1     = $_POST['street1'];
    $this->street2     = isset($_POST['street2']) ? $_POST['street2'] : "";
    $this->city        = $_POST['city'];
    $this->state       = $_POST['state'];
    $this->zipCode     = $_POST['zipCode'];

    if( $_POST['companyID'] !== null && $_POST['companyID'] !== "" )
    {
      $this->companyID = $_POST['companyID'];

      //update company
      $this->UpdateCompany();
    }else{//create

      if($this->CreateCompany() !== "error")
      {
        $this->CreateWordpress();
      }

      //update_post_meta( $post->ID, 'company_id', $this->CreateCompany() );
    }

  }

  /**
  **
  **/
  private function CreateCompany()
  {
    $ch = curl_init();

    $params = '{
      "@class": ".LogEventRequest",
      "eventKey": "CreateCompany",
      "companyName": "'. $this->companyName .'",
      "phoneNumber": "'. $this->phoneNumber .'",
      "street1": "'. $this->street1 .'",
      "street2": "'. $this->street2 .'",
      "city": "'. $this->city .'",
      "state": "'. $this->state .'",
      "zipCode": "'. $this->zipCode .'",
      "playerId": "'.playerID.'"
    }';

    curl_setopt($ch, CURLOPT_URL, "https://w373975yvdzr.preview.gamesparks.net/rs/device/MkfEiAZ5KZVUSpiohZF3g3GYr8iWXZFf/LogEventRequest");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_POST, 1);

    $headers = array();
    $headers[] = "Content-Type: application/json";
    $headers[] = "Accept: application/json";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }else{
      $this->Logs($result);
      $result = json_decode($result);
    }
    curl_close ($ch);
    if( isset($result->scriptData->new_company_ID) ){
      //$this->DataBase('insert');
      $this->companyID = $result->scriptData->new_company_ID;
      return;
    }else{
      return "error";
    }
    //return $result->scriptData->new_company_ID;
  }

  /**
  **
  **/
  private function UpdateCompany()
  {
    $ch = curl_init();

    $params = '{
      "@class": ".LogEventRequest",
      "eventKey": "EditCompany2",
      "companyId": "'. $this->companyID .'",
      "companyName": "'. $this->companyName .'",
      "phoneNumber": "'. $this->phoneNumber .'",
      "street1": "'. $this->street1 .'",
      "street2": "'. $this->street2 .'",
      "city": "'. $this->city .'",
      "state": "'. $this->state .'",
      "zipCode": "'. $this->zipCode .'",
      "playerId": "'.playerID.'"
    }';

    curl_setopt($ch, CURLOPT_URL, "https://w373975yvdzr.preview.gamesparks.net/rs/device/MkfEiAZ5KZVUSpiohZF3g3GYr8iWXZFf/LogEventRequest");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_POST, 1);

    $headers = array();
    $headers[] = "Content-Type: application/json";
    $headers[] = "Accept: application/json";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }else{
      $this->Logs($result);
      $result = json_decode($result);
      //return isset($result->scriptData->update)
    }
    curl_close ($ch);
    $this->DataBase("update");
  }

  private function CreateWordpress()
  {

    $user_id = get_current_user_id();

    $my_post = array(
      'post_title'    => $this->companyName,
      'post_status'   => 'publish',
      'post_author'   => $user_id,
      'post_type'     => "company"
    );

    // Insert the post into the database.
    $post_id = wp_insert_post( $my_post );

    update_post_meta( $post_id, 'company_id', $this->companyID );
    update_post_meta( $post_id, 'phoneNumber', $this->phoneNumber );
    update_post_meta( $post_id, 'street1', $this->street1 );
    update_post_meta( $post_id, 'street2', $this->street2 );
    update_post_meta( $post_id, 'city', $this->city );
    update_post_meta( $post_id, 'state', $this->state );
    update_post_meta( $post_id, 'zipCode', $this->zipCode );

    $perma =  get_permalink( $post_id );
    $perma = explode("/", $perma );
    if( isset($perma[5]) && $perma[5] !== "" ){
      $link = "https://" . $_SERVER['HTTP_HOST'] . "/{$perma[3]}/wp-admin/post.php?post={$post_id}&action=edit";
    }else{
      $link = "https://" . $_SERVER['HTTP_HOST'] . "/wp-admin/post.php?post={$post_id}&action=edit";
    }

    echo $link;
  }

  /**
  **
  **/
  private function DataBase( $task )
  {
    global $wpdb;

    if( $task == "insert" ){
      $wpdb->query( $wpdb->prepare( "INSERT INTO Company (companyID,companyName,phoneNumber,street1,street2,city,state,zipCode)
                                         VALUES ('%d','%s','%s','%s','%s','%s','%s','%s')",
                                         array($this->companyID,$this->companyName,$this->phoneNumber,$this->street1,$this->street2,$this->city,$this->state,$this->zipCode) ) );
    }else if( $task == "update" ){
      //check if exists on database otherwise insert again
      $res = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM Company WHERE companyID = %d",array($this->companyID) ) );

      if( sizeof($res) > 0 ){
        $wpdb->get_results( $wpdb->prepare( "UPDATE Company
                                             SET companyName = '%s', phoneNumber = '%s', street1 = '%s',street2 = '%s', city = '%s', state = '%s', zipCode = '%s'
                                             WHERE companyID = %d",array($this->companyName,$this->phoneNumber,$this->street1,$this->street2,$this->city,$this->state,$this->zipCode,$this->companyID) ) );
      }else{
        $wpdb->query( $wpdb->prepare( "INSERT INTO Company (companyID,companyName,phoneNumber,street1,street2,city,state,zipCode)
                                           VALUES ('%d','%s','%s','%s','%s','%s','%s','%s')",
                                           array($this->companyID,$this->companyName,$this->phoneNumber,$this->street1,$this->street2,$this->city,$this->state,$this->zipCode) ) );
      }

    }

  }

  /**
  ** Logs from gamesparks api
  **/
  private function Logs($request)
  {
    global $wpdb;

    $wpdb->query( "INSERT INTO Gamespark_logs (name,log,date) VALUES ('company','{$request}',NOW())" );

  }
}
