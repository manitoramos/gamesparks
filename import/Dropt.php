<?php

//ini_set( 'display_errors', 1 );

//INCLUDE WP-LOAD
//require_once __DIR__ . '/../../../../wp-load.php';

//require_once 'configprodution.php';

//new ImportDropts();

class ImportDropts
{
    private $token;

    function __construct ()
    {
        $this->AuthenticationProd();
        $this->Data();
        //$this->GetData( "dateTimesDiscarded", "DROPTdiscarded" );
    }

    /**
     * @param $field
     * @param $tablename
     * @return array|bool|mixed|object|string|null
     */
    private function GetData ($field, $tablename)
    {
        $params = '{
        "asc": true,
        "query":
            {
            "field": "' . $field . '",
            "operator": "NE",
            "value": "-1"
            }
        }';

        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, 'https://' . API_KEY . '.preview.cluster.gamesparks.net/restv2/game/' . API_KEY . '/data/'. $tablename .'/query' );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $params );
        curl_setopt( $ch, CURLOPT_POST, 1 );

        $headers = array();
        $headers[] = 'Content-Type: application/json;charset=UTF-8';
        $headers[] = 'Accept: application/json';
        $headers[] = 'X-Gs-Jwt: ' . $this->token . '';
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

        $result = curl_exec( $ch );
        if ( curl_errno( $ch ) ) {
            echo 'Error:' . curl_error( $ch );
        }
        curl_close( $ch );
        $result = json_decode( $result );
        if ( isset( $result->errorCode ) && $result->errorCode == "token_expired" ) {
            //get new token and repeat
            echo $result;
        } else if ( isset( $result->errorCode ) && $result->errorCode != "token_expired" ) {
            //other error
            $this->token;
            echo json_encode( $result );
        } else if ( !isset( $result->errorCode ) ) {
            return $result;
            echo json_encode( $result );
        }
        //exit();
    }

    /**
     *
     */
    private function Data ( )
    {
//data from tables gamesparks
        $captured  = $this->GetData( "dateTimeCaptured", "DROPTcaptured" );
        $discarded = $this->GetData( "discardedUserIDs", "DROPTdiscarded" );
        $links     = $this->GetData( 'subGroupID', 'DROPTlinks' );
        $redeem    = $this->GetData( 'redeemedUserID', 'DROPTredeemed' );


        echo json_encode( $links );
        exit();

        for($t = 0; $t < sizeof($links->results) ; $t++){
            if($links->results[$t]->id == 1){
                echo "asdas";
                exit();
            }
        }

        //create if not exists
        global $wpdb;
        //****************************************************
        // i = captured :: e = discarded :: f = links :: g = redeem *
        //****************************************************
        for ($i = 0; $i < sizeof( $captured->results ); $i++) {
            if ( $captured->results[$i]->id != "NaN" ) {
                $res = $wpdb->get_results( "SELECT * FROM SubGroups WHERE subGroupID = {$captured->results[$i]->id} " );
                if ( sizeof( $res ) != 0 ) {

                    for ($e = 0; $e < sizeof( $discarded->results ); $e++) {
                        //loop until get the same id on discarded
                        if ( $discarded->results[$e]->id == $captured->results[$i]->id ) {
                            //loop until get the same id on links
                            for ($f = 0; $f < sizeof( $links->results ); $f++) {
                                if ( $links->results[$f]->id == $captured->results[$i]->id ) {
                                    //loop until get the same id on $redeem
                                    for ($g = 0; $g < sizeof( $redeem->results ); $g++) {
                                        if ( $redeem->results[$g]->id == $captured->results[$i]->id ) {
                                            $this->CreateDropt( $captured->results[$i], $links->results[$f]->data, $discarded->results[$e]->data, $redeem->results[$g]->data );
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                    }

                }
            }
        }
    }

    /**
     * @param $captured
     * @param $links
     * @param $discarded
     * @param $redeem
     */
    private function CreateDropt ( $captured, $links, $discarded, $redeem )
    {
        global $wpdb;

        //CREATE ON OUR TABLES
        $created  = isset( $links->dateTimeCreated ) ? $links->dateTimeCreated : "";
        $modified = $links->dateTimeModified;
        $wpdb->query(
            $wpdb->prepare("INSERT INTO Dropt
                (droptID, capturedUserID, dateTimeCaptured, capturedLatitude, capturedLongitude, dateTimesDiscarded, discardedUserIDs,
                 expired, subGroupID, status, redeemedUserID, redeemedLocationIDs, dateTimesRedeemed, offerCodeImageURL,
                 dateTimeCreated, dateTimeModified)
                VALUES ('%d','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                array( $captured->id, $captured->data->capturedUserID, $captured->data->dateTimeCaptured,
                    $captured->data->capturedLatitude, $captured->data->capturedLongitude, $discarded->dateTimesDiscarded,
                    $discarded->discardedUserIDs, $links->expired, $links->subGroupID, $links->status,
                    $redeem->redeemedUserID, $redeem->redeemedLocationIDs, $redeem->dateTimesRedeemed,
                    $redeem->offerCodeImageURL, $created, $modified ) ) );
    }

    /**
     * @return string
     */
    private function Authentication ()
    {
        // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, "https://auth.gamesparks.net/restv2/auth/game/" . API_KEY . "/jwt" );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );


        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: Basic dmRlbGdhZG9AbW9iaW50ZWcuY29tOnF1ZWlqbzEyMyE=';
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

        $result = curl_exec( $ch );
        if ( curl_errno( $ch ) ) {
            echo 'Error:' . curl_error( $ch );
        }
        curl_close( $ch );
        //echo $result;
        $result = (array)json_decode( $result );
        //token
        if ( isset( $result["X-GS-JWT"] ) ) {
            $this->token = $result["X-GS-JWT"];
        } else {
            return "error";
        }
    }


    private function AuthenticationProd ()
    {
        // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, "https://auth.gamesparks.net/restv2/auth/game/" . API_KEY . "/jwt" );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );


        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: Basic amFzb24ua2VsbGV5QGRyb3B0aW5jLmNvbTpXb3JsZHJhY2UxMjMj';
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

        $result = curl_exec( $ch );
        if ( curl_errno( $ch ) ) {
            echo 'Error:' . curl_error( $ch );
        }
        curl_close( $ch );
        //echo $result;
        $result = (array)json_decode( $result );
        //token
        if ( isset( $result["X-GS-JWT"] ) ) {
            $this->token = $result["X-GS-JWT"];
        } else {
            return "error";
        }
    }


}

?>
