<?php

/**
 * GameSparks
 */
class Dropt extends ControllerSparks
{
    //main vars
    private $droptID;
    private $companyID;
    private $campaignID;
    private $groupName;
    private $groupID;
    private $droptName;
    private $redemptionIDs;
    private $termsAndConditions;
    private $singleDroptRules;
    private $groupRules;
    private $Rules;
    private $qualifyCount;
    private $visibleUserEmails;
    private $dateTimeCaptureBegin;
    private $dateTimeCaptureEnd;
    private $dateTimeRedeemBegin;
    private $dateTimeRedeemEnd;
    private $geoLocationIDs;
    private $radiusinMiles;
    private $duplicateThisMany;

    //image vars
    private $offerCodes;
    private $parachutePayload;
    private $parachuteOffer;
    private $capturedSuccess;
    private $whatsDroppinOffer;
    private $walletIcon;
    private $contestHeaderOffer;
    private $contestHeaderIcon;
    private $contestProgressIcon;

    function __construct()
    {
        $this->Configs( "prod" );

        global $post,$wpdb;

        //get the title
        $title = $wpdb->get_results("select * from {$wpdb->prefix}posts where ID = {$post->ID}");
        $this->droptName = $title[0]->post_title;
        //values to vars
        $this->companyID = $this->GetValueID( $post->ID, "company_id", "company_dropt" );//get_field('company_dropt');
        $this->groupName = get_field('groupName');
        $this->campaignID = $this->GetValueID( $post->ID, "campaign_id", "campaign_dropt" );//get_field('campaign_dropt');
        $this->redemptionIDs = $this->GetRedemptionID( $post->ID );//get_field('redemptionLocationIDs_dropt');
        $this->termsAndConditions = get_field('termsAndConditions');
        $this->Rules = get_field('rules');
        $this->qualifyCount = get_field('qualify_count');
        $this->visibleUserEmails = get_field('visibleUserEmails');
        $this->dateTimeCaptureBegin = $this->replace(get_field('dateTimeCaptureBegin'));
        $this->dateTimeCaptureEnd = $this->replace(get_field('dateTimeCaptureEnd'));
        $this->dateTimeRedeemBegin = $this->replace(get_field('dateTimeRedeemBegin'));
        $this->dateTimeRedeemEnd = $this->replace(get_field('dateTimeRedeemEnd'));
        $this->geoLocationIDs = $this->GetValueID( $post->ID, "geolocation_id", "geoLocation_dropt" );//get_field('geoLocation_dropt');
        $this->radiusinMiles = get_field('radiusInMiles');
        $this->duplicateThisMany = get_field('duplicateThisMany');
        $this->MakeRules( $post->ID );

        //images vars
        $this->offerCodes = $this->GetImage("offerCodes");
        $this->parachutePayload = $this->GetImage("parachutePayload");
        $this->parachuteOffer = $this->GetImage("parachuteOffer");
        $this->capturedSuccess = $this->GetImage("capturedSuccess");
        $this->whatsDroppinOffer = $this->GetImage("whatsDroppinOffer");
        $this->walletIcon = $this->GetImage("walletIcon");
        $this->contestHeaderOffer = $this->GetImage("contestHeaderOffer");
        $this->contestHeaderIcon = $this->GetImage("contestHeaderIcon");
        $this->contestProgressIcon = $this->GetImage("contestProgressIcon");
        //https://s3.amazonaws.com/dropt-app/wp/wp-content/uploads/2018/07/16171510/REIbarcode.png
        update_post_meta($post->ID, 'dropt_id', $this->CreateDropt());

    }

    /**
     **
     **/
    private function CreateDropt()
    {
        global $post;

        $ch = curl_init();

        $params = '{
          "@class": ".LogEventRequest",
          "eventKey": "CreateDropt",
          "sponsorCompanyID": "' . $this->companyID . '",
          "campaignID": "' . $this->campaignID . '",
          "groupName": "' . $this->groupName . '",
          "droptName": "' . $this->droptName . '",
          "redemptionLocationIDs": "' . $this->redemptionIDs . '",
          "termsAndConditions": "' . $this->termsAndConditions . '",
          "singleDroptRules": "' . $this->singleDroptRules . '",
          "groupRules": "' . $this->groupRules . '",
          "visibleUserEmails": "' . $this->visibleUserEmails . '",
          "dateTimeCaptureBegin": "' . $this->dateTimeCaptureBegin . '",
          "dateTimeCaptureEnd": "' . $this->dateTimeCaptureEnd . '",
          "dateTimeRedeemBegin": "' . $this->dateTimeRedeemBegin . '",
          "dateTimeRedeemEnd": "' . $this->dateTimeCaptureEnd . '",
          "droptImageURLs": {
            "offerCodes": "' . $this->offerCodes . '",
            "parachutePayload": "' . $this->parachutePayload . '",
            "parachuteOffer": "' . $this->parachuteOffer . '",
            "capturedSuccess": "' . $this->capturedSuccess . '",
            "whatsDroppinOffer": "' . $this->whatsDroppinOffer . '",
            "walletIcon": "' . $this->walletIcon . '",
            "contestHeaderOffer": "' . $this->contestHeaderOffer . '",
            "contestHeaderIcon": "' . $this->contestHeaderIcon . '",
            "contestProgressIcon": "' . $this->contestProgressIcon . '"
          },
          "geoLocationIDs": "' . $this->geoLocationIDs . '",
          "radiusInMiles": "' . $this->radiusinMiles . '",
          "duplicateThisMany": "' . $this->duplicateThisMany . '",
          "playerId": "' . playerID . '"
        }';

        //$this->Logs($params);

        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        $headers[] = "Content-Type: application/json";
        $headers[] = "Accept: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        } else {
            $this->Logs( $result );
            update_post_meta( $post->ID, 'box_log', $result );
            update_post_meta( $post->ID, 'query_log', $params );
            $result = json_decode($result);
        }
        curl_close($ch);
        if ($result->scriptData->group_id) {
            $this->groupID = $result->scriptData->group_id;
            $this->CreateGroup("insert");
            return "success";
        } else {
            $my_post = array(
                'ID'           => $post->ID,
                'post_status'   => 'error',
            );
            wp_update_post( $my_post );
            return "error";
        }
    }

    /**
     ** GET VALUE POST ID
     **/
    private function GetValueID( $post_id, $id_metakey, $drop_metakey )
    {
        global $wpdb;
        $res = $wpdb->get_results("SELECT
                                  metaid.meta_value
                                  FROM {$wpdb->prefix}postmeta as metadrop
                                  INNER JOIN {$wpdb->prefix}postmeta as metaid on metaid.post_id = metadrop.meta_value AND metaid.meta_key = '$id_metakey'
                                  WHERE metadrop.post_id = {$post_id} AND metadrop.meta_key = '$drop_metakey'");
        return $res[0]->meta_value;
    }

    /**
     ** GET Redemption ID
     **/
    private function GetRedemptionID( $post_id )
    {
        $red_ids = [];
        $ids = get_post_meta( $post_id, 'redemptionLocationIDs_dropt', true );
        foreach ($ids as $id) {
          $red_ids[] = implode( "", get_post_meta( $id , "redemption_id" ) );
        }
        $red_ids = implode( ",", $red_ids );//array to string
        return $red_ids;
    }

    /**
     ** Replace the dates to get the right model
     **/
    private function replace($string)
    {
        $string = str_replace(' ', '-', $string);
        $string = str_replace(':', '-', $string);
        return $string;
    }

    /**
     ** Create GroupID on Database
     **/
    private function CreateGroup($task)
    {
        global $wpdb;

        if ($task == "insert") {
            $wpdb->query($wpdb->prepare("INSERT INTO Groups (groupID,groupName,dateTimeCreated,dateTimeModified)
                                     VALUES ('%d','%s','%s','%s')",
                array($this->groupID, $this->groupName, date('Y-m-d H:i:s'), date('Y-m-d H:i:s'))));
        } else if ($task == "update") {
            //check if exists on database otherwise insert again
            $res = $wpdb->get_results($wpdb->prepare("SELECT * FROM Groups WHERE groupID = %d", array($this->prizeID)));
            if (isset($res[0])) {
                $wpdb->get_results($wpdb->prepare("UPDATE Groups
                                             SET groupName = '%s', dateTimeModified = '%s',
                                             WHERE groupID = %d", array($this->groupName, date('Y-m-d H:i:s'), $this->groupID)));
            } else {
                $wpdb->query($wpdb->prepare("INSERT INTO Groups (groupID,groupName,dateTimeCreated,dateTimeModified)
                                       VALUES ('%d','%s','%s','%s')",
                    array($this->groupID, $this->groupName, date('Y-m-d H:i:s'), date('Y-m-d H:i:s'))));
            }

        }
    }

    /**
     ** Create Dropt on Database
     **/
    private function DataBase()
    {
        global $wpdb;
        //need to save the images too
        $wpdb->query($wpdb->prepare("INSERT INTO DROPT (sponsorCompanyID,campaignID,groupName,droptName,redemptionLocationIDs,termsAndConditions,singleDroptRules,
                                   groupRules,visibleUserEmails,dateTimeCaptureBegin,dateTimeCaptureEnd,dateTimeCaptureBegin,dateTimeRedeemEnd,
                                   geoLocationIDs,radiusinMiles,duplicateThisMany,dateTimeCreated,dateTimeModified)
                                   VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
            array($this->sponsorCompanyID, $this->campaignID, $this->groupName, $this->groupName, $this->droptName, $this->redemptionIDs,
                $this->termsAndConditions, $this->singleDroptRules, $this->groupRules, $this->visibleUserEmails, $this->dateTimeCaptureBegin,
                $this->dateTimeCaptureEnd, $this->dateTimeRedeemBegin, $this->dateTimeRedeemEnd, $this->geoLocationIDs, $this->radiusinMiles,
                $this->duplicateThisMany, date('Y-m-d H:i:s'), date('Y-m-d H:i:s'))));

    }

    /**
     ** meta_key
     **/
    private function GetImage(string $meta_key)
    {
        global $wpdb;
        global $post;

        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}postmeta WHERE post_id = {$post->ID} AND meta_key = 'droptImagesURLs_0_{$meta_key}'");
        //post id image
        $value = $res[0]->meta_value;
        if (!isset($value) || $value === "" || $value === null) {
            return "";
        }

        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}postmeta WHERE post_id = {$value} AND meta_key = '_wp_attached_file'");
        //image url
        return get_site_url() . "/wp-content/uploads/" . $res[0]->meta_value;
    }

    // private function MakeRules()
    // {
    //     if ($this->Rules === "regular") {
    //         $this->singleDroptRules = "capture_anywhere,infinite_redeems";
    //     } else if ($this->Rules === "limited") {
    //         $this->singleDroptRules = "daily_limit_15";
    //     } else if ($this->Rules === "daily") {
    //         $this->singleDroptRules = "capture_anywhere,infinite_captures";
    //         $this->groupRules = "capture_daily,qualify_count_" . $this->qualifyCount;
    //     } else if ($this->Rules === "scavenger") {
    //         $this->singleDroptRules = "infinite_captures";
    //         $this->groupRules = "qualify_count_" . $this->qualifyCount;
    //     } else if ($this->Rules === "prize") {
    //         $this->singleDroptRules = "immediate_prize";
    //         $this->groupRules = "qualify_count_" . $this->qualifyCount;
    //         $this->geoLocationIDs = -1;
    //     }
    // }

    /**
     ** Logs from gamesparks api
     **/
    private function Logs($request)
    {
        global $wpdb;

        $wpdb->query("INSERT INTO Gamespark_logs (name,log,date) VALUES ('dropt','{$request}',NOW())");

    }

    /**
    ** Rules
    **/
    private function MakeRules( $post_id )
    {

      $rules = get_post_meta( $post_id, 'rules', true );
      $prizes = get_post_meta( $post_id, 'prize', true );
      for( $i = 0 ; $i < sizeof($rules);$i++ ){
        if( $rules[$i] == "qualify_count" ){
          unset( $rules[$i] );
          $this->groupRules = "qualify_count_" . $this->qualifyCount;
        }
      }
      if( ( $key = array_search( "immediate_prize", $prizes ) ) != false ) $this->geoLocationIDs = -1;
      $rules = implode( ",", $rules );
      $prizes = implode( ",", $prizes );

      if( empty($prizes) ){
        $this->singleDroptRules = $rules;
      }else if( empty($rules) ){
        $this->singleDroptRules = $prizes;
      }else{
        $this->singleDroptRules = $rules . "," . $prizes;
      }


    }
}
