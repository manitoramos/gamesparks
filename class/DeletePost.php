<?php
/**
 * Created by PhpStorm.
 * User: tiago
 * Date: 2/13/19
 * Time: 6:44 PM
 */

ini_set('display_errors', 1);

//INCLUDE WP-LOAD
require_once __DIR__ . '/../../../../wp-load.php';

//AUTH
require_once('../services/Authentication.php');
require_once('Controller.php');

class DeletePost extends ControllerSparks
{

    private $post_id;
    private $metakey;

    function __construct ()
    {

        $this->Configs("prod");
        $this->metakey = $_POST['action'];
        $this->post_id = $_POST['postid'];
        //if the post is draft didnt need to delete on gamesparks
        $this->CheckPost();

        $spark_id = $this->GetSparkID();
        $this->DeletePostSparks( $spark_id );
        $this->DeletePostFromTable( $spark_id );
    }

    /**
     * @return mixed
     */
    private function GetSparkID(  )
    {

        global $wpdb;

        $res = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta
                                   WHERE meta_key = '{$this->metakey}_id' AND post_id = {$this->post_id}" );

        return $res[0]->meta_value ;

    }

    private function DeletePostSparks( $spark_id )
    {
        global $post;

        $ch = curl_init();

        $params = '{
          "@class": ".LogEventRequest",
          "eventKey": "Delete",
          "deletefrom": "'. $this->metakey .'",
          "deleteid": "'. $spark_id .'",
          "playerId": "' . playerID . '"
        }';

        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        $headers[] = "Content-Type: application/json";
        $headers[] = "Accept: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        } else {
            $result = json_decode($result);

            if( $result->scriptData->STATUS == "deleted" ){
                echo "delete";
            }elseif( $result->error->ERROR == "id_not_exists" ){
                echo "id_not_exists";
            }else{
                //something wrong or the id didnt exists
                //echo $params;

                echo json_encode($result);
            }
        }
        curl_close($ch);
    }

    /**
     * DELETE IN OURS SAVE DATA TABLE
     * @param spark_id -- id from gamesparks
     */
    private function DeletePostFromTable( $spark_id )
    {
        global $wpdb;

        if( $this->metakey == "company" ){
            $wpdb->query( $wpdb->prepare( "DELETE FROM Company WHERE companyID = '%d'", [$spark_id] ) );
        }else if( $this->metakey == "campaign" ){
            $wpdb->query( $wpdb->prepare( "DELETE FROM Campaign WHERE campaignID = '%d'", [$spark_id] ) );
        }else if( $this->metakey == "geolocation" ){
            $wpdb->query( $wpdb->prepare( "DELETE FROM GeoLocation WHERE geoLocationID = '%d'", [$spark_id] ) );
        }else if( $this->metakey == "redemption" ){
            $wpdb->query( $wpdb->prepare( "DELETE FROM RedemptionLocation WHERE redemptionID = '%d'", [$spark_id] ) );
        }else if( $this->metakey == "dropt" ){

        }else if( $this->metakey == "prize" ){

        }

    }

    /**
     * Delete the post if is draft
     */
    private function CheckPost()
    {
        global $wpdb;

        $res = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}posts WHERE ID = {$this->post_id}" );

        if( sizeof($res) > 0 ){
            if( $res[0]->post_status == "draft" ) {
                echo "id_not_exists";
                exit();
            }
        }
    }

}

new DeletePost();
