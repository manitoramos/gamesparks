<?php

/**
 * GameSparks
 */
class Company extends ControllerSparks
{
    private $companyID;
    private $companyName;
    private $phoneNumber;
    private $street1;
    private $street2;
    private $city;
    private $state;
    private $zipCode;

    function __construct()
    {
        $this->Configs( "prod" );

        global $post, $wpdb;

        //get the title
        $title = $wpdb->get_results("select * from {$wpdb->prefix}posts where ID = {$post->ID}");
        $this->companyName = $title[0]->post_title;
        //values to vars
        $this->phoneNumber = get_field('phoneNumber');
        $this->street1 = get_field('street1');
        $this->street2 = get_field('street2');
        $this->city = get_field('city');
        $this->state = get_field('state');
        $this->zipCode = get_field('zipCode');

        if (get_field('company_id') !== null && get_field('company_id') !== "") {
            $this->companyID = get_field('company_id');

            //update company
            $this->UpdateCompany();
        } else {//create
            update_post_meta($post->ID, 'company_id', $this->CreateCompany());
        }

    }

    /**
     **
     **/
    private function CreateCompany()
    {
        global $post;

        $ch = curl_init();

        $params = '{
          "@class": ".LogEventRequest",
          "eventKey": "CreateCompany",
          "companyName": "' . $this->companyName . '",
          "phoneNumber": "' . $this->phoneNumber . '",
          "street1": "' . $this->street1 . '",
          "street2": "' . $this->street2 . '",
          "city": "' . $this->city . '",
          "state": "' . $this->state . '",
          "zipCode": "' . $this->zipCode . '",
          "playerId": "' . playerID . '"
        }';

        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        $headers[] = "Content-Type: application/json";
        $headers[] = "Accept: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        } else {
            $this->Logs($result);
            update_post_meta($post->ID, 'box_log', $result);
            update_post_meta( $post->ID, 'query_log', $params );
            $result = json_decode($result);
            $this->companyID = $result->scriptData->new_company_ID;
        }
        curl_close($ch);
        if( isset( $this->companyID ) && $this->companyID != "" ){
            $this->DataBase('insert');
        } else{
          $my_post = array(
              'ID'           => $post->ID,
              'post_status'   => 'error',
          );
          wp_update_post( $my_post );
        }
        return $result->scriptData->new_company_ID;
    }



    /**
     **
     **/
    private function UpdateCompany()
    {
        global $post,$wpdb;

        $ch = curl_init();

        //dev EditCompany2
        //prod EditCompany

        $params = '{
          "@class": ".LogEventRequest",
          "eventKey": "EditCompany",
          "companyId": "' . $this->companyID . '",
          "companyName": "' . $this->companyName . '",
          "phoneNumber": "' . $this->phoneNumber . '",
          "street1": "' . $this->street1 . '",
          "street2": "' . $this->street2 . '",
          "city": "' . $this->city . '",
          "state": "' . $this->state . '",
          "zipCode": "' . $this->zipCode . '",
          "playerId": "' . playerID . '"
        }';

        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        $headers[] = "Content-Type: application/json";
        $headers[] = "Accept: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        } else {
            $this->Logs($result);
            update_post_meta($post->ID, 'box_log', $result);
            update_post_meta( $post->ID, 'query_log', $params );
            $result = json_decode($result);
            //return isset($result->scriptData->update)
        }
        curl_close($ch);
        if( $result->scriptData->update == "success" ){
            $this->DataBase("update");
        } else{
            $res = $wpdb->get_results( "SELECT * FROM Company WHERE companyID = {$this->companyID}" );
            //on error put the data to the same old value
            update_post_meta( $post->ID, 'company_id', $this->companyID );
            update_post_meta( $post->ID, 'phoneNumber', $res[0]->phoneNumber );
            update_post_meta( $post->ID, 'street1', $res[0]->street1 );
            update_post_meta( $post->ID, 'street2', $res[0]->street2 );
            update_post_meta( $post->ID, 'city', $res[0]->city );
            update_post_meta( $post->ID, 'state', $res[0]->state );
            update_post_meta( $post->ID, 'zipCode', $res[0]->zipCode );
            wp_update_post( array('ID' => $post->ID, 'post_title' => "{$res[0]->companyName}" ) );
        }
    }

    /**
     ** Create only if created on gamesparks
     **/
    private function DataBase($task)
    {
        global $wpdb;

        if ($task == "insert") {
            $wpdb->query($wpdb->prepare("INSERT INTO Company (companyID,companyName,phoneNumber,street1,street2,city,state,zipCode)
                                     VALUES ('%d','%s','%s','%s','%s','%s','%s','%s')",
                array($this->companyID, $this->companyName, $this->phoneNumber, $this->street1, $this->street2, $this->city, $this->state, $this->zipCode)));
        } else if ($task == "update") {
            //check if exists on database otherwise insert again
            $res = $wpdb->get_results($wpdb->prepare("SELECT * FROM Company WHERE companyID = %d", array($this->companyID)));

            if (isset($res[0])) {
                $wpdb->get_results($wpdb->prepare("UPDATE Company
                                             SET companyName = '%s', phoneNumber = '%s', street1 = '%s',street2 = '%s', city = '%s', state = '%s', zipCode = '%s'
                                             WHERE companyID = %d", array($this->companyName, $this->phoneNumber, $this->street1, $this->street2, $this->city, $this->state, $this->zipCode, $this->companyID)));
            } else {
                $wpdb->query($wpdb->prepare("INSERT INTO Company (companyID,companyName,phoneNumber,street1,street2,city,state,zipCode)
                                       VALUES ('%d','%s','%s','%s','%s','%s','%s','%s')",
                    array($this->companyID, $this->companyName, $this->phoneNumber, $this->street1, $this->street2, $this->city, $this->state, $this->zipCode)));
            }

        }

    }

    /**
     ** Logs from gamesparks api
     **/
    private function Logs($request)
    {
        global $wpdb;

        $wpdb->query("INSERT INTO Gamespark_logs (name,log,date) VALUES ('company','{$request}',NOW())");

    }
}
