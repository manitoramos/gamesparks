<?php

add_action("admin_init", "redemption_custom_metabox");

function redemption_custom_metabox( ){
    add_meta_box( "redemption", "Redemption ID", "redemption_metabox", "redemptionlocation", "side", "low" );
}

function redemption_metabox(){
    global $post;

    $data = get_post_custom( $post->ID );
    $val = isset( $data['redemption_id'] ) ? esc_attr( $data['redemption_id'][0] ) : null;
    echo "<small>Automatic Generated after publish the post</small>";
    echo "<input type='text' name='redemption_id' id='redemption_id' style='width:100%;' value='{$val}' disabled />";
}


/**
** Save Post
**/
add_action( "save_post_redemptionlocation","save_redemption" );

function save_redemption( ){
    global $post;
    global $trashpost;

    //on untrash didnt make the post function
    if( isset($_GET['action']) && $_GET['action'] == 'untrash' ){
      $previous = "javascript:history.go(-1)";
      if(isset($_SERVER['HTTP_REFERER'])) {
          $previous = $_SERVER['HTTP_REFERER'];
      }
      header('Location: '.$previous);
      return;
    }

    if($trashpost == 1){
      return;
    }

    static $updated = false;

    if( $updated ) {
      return;
    }

    //if the post is not created yet
    if( !isset( $post ) ){
      return;
    }

    // if( define('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
    //   return $post->ID;
    // }
    if( isset( $post->ID ) ){

        $updated = true;

        new Redemption();

    }
}

/**
 * GameSparks
 */
class Redemption
{
  private $redemptionID;
  private $locationName;
  private $companyID;
  private $phoneNumber;
  private $street1;
  private $street2;
  private $city;
  private $state;
  private $zipCode;
  private $radiusinMiles;
  private $geoLocationID;

  function __construct()
  {
    global $post;

    //values to vars
    $this->locationName= $post->post_title;
    $this->companyID   = 12;
    $this->phoneNumber = get_post_meta( $post->ID, 'phonenumber', true);
    $this->street1     = get_post_meta( $post->ID, 'street1', true);
    $this->street2     = get_post_meta( $post->ID, 'street2', true);
    $this->city        = get_post_meta( $post->ID, 'city', true);
    $this->state       = get_post_meta( $post->ID, 'state', true);
    $this->zipCode     = get_post_meta( $post->ID, 'zipCode', true);
    $this->radiusinMiles= get_post_meta( $post->ID, 'radiusInMiles', true);
    $this->geoLocationID= 2;
    //$this->geoLocationID= explode('"', get_post_meta( $post->ID, 'geoLocation_redemption_location', true ))[1];

    if( get_post_meta( $post->ID, 'redemption_id', true ) !== null && get_post_meta( $post->ID, 'redemption_id', true ) !== "" )
    {
      $this->redemptionID = get_post_meta( $post->ID, 'redemption_id', true );

      //update geolocation
      $this->UpdateRedemption();
    }else{//create
      update_post_meta( $post->ID, 'redemption_id', $this->CreateRedemption() );
      //exit();
    }

  }

  /**
  **
  **/
  private function CreateRedemption()
  {
    $ch = curl_init();

    $params = '{
      "@class": ".LogEventRequest",
      "eventKey": "CreateRedemptionLocation",
      "companyID": "'. $this->companyID .'",
      "locationName": "'. $this->locationName .'",
      "phoneNumber": "'. $this->phoneNumber .'",
      "street1": "'. $this->street1 .'",
      "street2": "'. $this->street2 .'",
      "city": "'. $this->city .'",
      "state": "'. $this->state .'",
      "zipCode": "'. $this->zipCode .'",
      "radiusinMiles": "'. $this->radiusinMiles .'",
      "geoLocationID": "'. $this->geoLocationID .'",
      "playerId": "'.playerID.'"
    }';

    curl_setopt($ch, CURLOPT_URL, "https://w373975yvdzr.preview.gamesparks.net/rs/device/MkfEiAZ5KZVUSpiohZF3g3GYr8iWXZFf/LogEventRequest");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_POST, 1);

    $headers = array();
    $headers[] = "Content-Type: application/json";
    $headers[] = "Accept: application/json";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }else{
      $this->Logs($result);
      $result = json_decode($result);
    }
    curl_close ($ch);
    if( isset($result->scriptData->new_redemption_location_ID) ){
      $this->redemptionID = intval($result->scriptData->new_redemption_location_ID);
      $this->DataBase('insert');
      return $result->scriptData->new_redemption_location_ID;
    }
  }

  /**
  **
  **/
  private function UpdateRedemption()
  {
    try {
      $ch = curl_init();

      $params = '{
        "@class": ".LogEventRequest",
        "eventKey": "EditRedemptionLocation",
        "redemptionID": "'. $this->redemptionID .'",
        "companyID": "'. $this->companyID .'",
        "locationName": "'. $this->locationName .'",
        "phoneNumber": "'. $this->phoneNumber .'",
        "street1": "'. $this->street1 .'",
        "street2": "'. $this->street2 .'",
        "city": "'. $this->city .'",
        "state": "'. $this->state .'",
        "zipCode": "'. $this->zipCode .'",
        "radiusInMiles": "'. $this->radiusinMiles .'",
        "geoLocationID": "'. $this->geoLocationID .'",
        "playerId": "'.playerID.'"
      }';

      curl_setopt($ch, CURLOPT_URL, "https://w373975yvdzr.preview.gamesparks.net/rs/device/MkfEiAZ5KZVUSpiohZF3g3GYr8iWXZFf/LogEventRequest");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      curl_setopt($ch, CURLOPT_POST, 1);

      $headers = array();
      $headers[] = "Content-Type: application/json";
      $headers[] = "Accept: application/json";
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      $result = curl_exec($ch);
      if (curl_errno($ch)) {
          echo 'Error:' . curl_error($ch);
          $this->Logs(curl_error($ch));
      }else{
        //return isset($result->scriptData->update)
      }
      curl_close ($ch);
      $this->Logs($result);
      $result = json_decode($result);
      $this->DataBase("update");
    } catch (Exception $e) {
      $this->Logs($e);
    }
  }

  /**
  **
  **/
  private function DataBase( $task )
  {
    global $wpdb;

    if( $task == "insert" ){
        $wpdb->query( $wpdb->prepare( "INSERT INTO RedemptionLocation (redemptionID,companyID,locationName,phoneNumber,street1,street2,city,
                                       state,zipCode,radiusinMiles,geoLocationID,dateTimeCreated,dateTimeModified)
                                       VALUES ('%d','%d','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                       array($this->redemptionID,$this->companyID,$this->locationName,$this->phoneNumber,$this->street1,
                                       $this->street2,$this->city,$this->state,$this->zipCode,$this->radiusinMiles,$this->geoLocationID,date('Y-m-d H:i:s'),date('Y-m-d H:i:s')) ) );
        if($wpdb->last_error !== ''){
          $this->Logs($wpdb->last_result());
          $this->Logs($wpdb->last_error());
        }
    }else if( $task == "update" ){
      //check if exists on database otherwise insert again
      $res = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM RedemptionLocation WHERE redemptionID = %d",array($this->redemptionID) ) );

      if( sizeof($res) > 0 ){
        $wpdb->get_results( $wpdb->prepare( "UPDATE RedemptionLocation
                                             SET companyID = '%d', locationName = '%s', phoneNumber = '%s', street1 = '%s', street2 = '%s', city = '%s', state = '%s',
                                             zipCode = '%s', radiusinMiles = '%s', geoLocationID = '%s', dateTimeModified = '%s'
                                             WHERE redemptionID = %d",
                                             array($this->companyID,$this->locationName,$this->phoneNumber,$this->street1,$this->street2,$this->city,$this->state,$this->zipCode,
                                             $this->radiusinMiles,$this->geoLocationID,date('Y-m-d H:i:s'),$this->redemptionID) ) );
      }else{
        $wpdb->query( $wpdb->prepare( "INSERT INTO RedemptionLocation (redemptionID,companyID,locationName,phoneNumber,street1,street2,city,
                                       state,zipCode,radiusinMiles,geoLocationID,dateTimeCreated,dateTimeModified)
                                       VALUES ('%d','%d','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                                       array($this->redemptionID,$this->companyID,$this->locationName,$this->phoneNumber,$this->street1,
                                       $this->street2,$this->city,$this->state,$this->zipCode,$this->radiusinMiles,$this->geoLocationID,date('Y-m-d H:i:s'),date('Y-m-d H:i:s')) ) );
      }

    }

  }

  /**
  ** Logs from gamesparks api
  **/
  private function Logs($request)
  {
    global $wpdb;

    $wpdb->query( "INSERT INTO Gamespark_logs (name,log,date) VALUES ('redemption','{$request}',NOW())" );

  }
}
