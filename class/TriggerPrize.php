<?php

/**
 * GameSparks
 */
class TriggerPrize extends ControllerSparks
{
    private $prizeID;
    private $groupID;
    private $winnerUserID;
    private $filterType;

    function __construct()
    {
        global $post;

        $this->Configs( "prod" );

        //values to vars
        $this->groupID = $_POST['group_id'];
        $this->winnerUserID = get_field('manualWinnerUserID');
        $this->filterType = get_field('winnerFilterType');

        if (get_field('prize_id') !== null && get_field('prize_id') !== "") {
            $this->campaignID = get_field('prize_id');

            //update geolocation
            $this->UpdatePrize();
        } else {//create
            update_post_meta($post->ID, 'prize_id', $this->CreatePrize());
            update_post_meta( $post->ID, 'group_id', $this->groupID );
            //exit();
        }

    }

    /**
     **
     **/
    private function CreatePrize()
    {
        global $post;

        $ch = curl_init();

        $params = '{
          "@class": ".LogEventRequest",
          "eventKey": "ManualTrigerPrize",
          "groupID": "' . $this->groupID . '",
          "manualWinnerUserID": "' . $this->winnerUserID . '",
          "winnerFilterType": "' . $this->filterType . '",
          "playerId": "' . playerID . '"
        }';

        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        $headers[] = "Content-Type: application/json";
        $headers[] = "Accept: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        } else {
            $this->Logs($result);
            update_post_meta($post->ID, 'box_log', $result);
            update_post_meta( $post->ID, 'query_log', $params );
            $result = json_decode($result);
        }
        curl_close($ch);
        $this->DataBase('insert');
    }

    // private function UpdatePrize()
    // {
    //     global $post;
    //
    //     try {
    //         $ch = curl_init();
    //
    //         $params = '{
    //           "@class": ".LogEventRequest",
    //           "eventKey": "EditCampaign",
    //           "campaignID": "' . $this->campaignID . '",
    //           "companyID": "' . $this->companyID . '",
    //           "globalGroupRule": "' . $this->rule . '",
    //           "playerId": "' . playerID . '"
    //         }';
    //
    //         curl_setopt($ch, CURLOPT_URL, "https://w373975yvdzr.preview.gamesparks.net/rs/device/MkfEiAZ5KZVUSpiohZF3g3GYr8iWXZFf/LogEventRequest");
    //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //         curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    //         curl_setopt($ch, CURLOPT_POST, 1);
    //
    //         $headers = array();
    //         $headers[] = "Content-Type: application/json";
    //         $headers[] = "Accept: application/json";
    //         curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    //
    //         $result = curl_exec($ch);
    //         if (curl_errno($ch)) {
    //             echo 'Error:' . curl_error($ch);
    //             $this->Logs(curl_error($ch));
    //         } else {
    //             update_post_meta($post->ID, 'box_log', $result);
    //             //return isset($result->scriptData->update)
    //         }
    //         curl_close($ch);
    //         $this->Logs($result);
    //         $result = json_decode($result);
    //         $this->DataBase("update");
    //     } catch (Exception $e) {
    //         $this->Logs($e);
    //     }
    // }

    /**
     **
     **/
    private function DataBase($task)
    {
        global $wpdb;

        if ($task == "insert") {
            $wpdb->query($wpdb->prepare("INSERT INTO Prize (groupID,manualWinnerUserID,winnerFilterType,dateTimeCreated,dateTimeModified)
                                     VALUES ('%d','%d','%d','%s','%s','%s')",
                array($this->groupID, $this->manualWinnerUserID, $this->winnerFilterType, date('Y-m-d H:i:s'), date('Y-m-d H:i:s'))));
        }
        // } else if ($task == "update") {
        //     //check if exists on database otherwise insert again
        //     $res = $wpdb->get_results($wpdb->prepare("SELECT * FROM Prize WHERE prizeID = %d", array($this->prizeID)));
        //     if (isset($res[0])) {
        //         $wpdb->get_results($wpdb->prepare("UPDATE Prize
        //                                      SET groupID = '%d', manualWinnerUserID = '%d', winnerFilterType = '%s', dateTimeModified = '%s',
        //                                      WHERE prizeID = %d", array($this->groupID, $this->manualWinnerUserID, $this->winnerFilterType, date('Y-m-d H:i:s'), $this->prizeID)));
        //     } else {
        //         $wpdb->query($wpdb->prepare("INSERT INTO Prize (prizeID,groupID,manualWinnerUserID,winnerFilterType,dateTimeCreated,dateTimeModified)
        //                                VALUES ('%d','%d','%d','%s','%s','%s')",
        //             array($this->prizeID, $this->groupID, $this->manualWinnerUserID, $this->winnerFilterType, date('Y-m-d H:i:s'), date('Y-m-d H:i:s'))));
        //     }
        //
        // }

    }

    /**
     ** Logs from gamesparks api
     **/
    private function Logs($request)
    {
        global $wpdb;

        $wpdb->query("INSERT INTO Gamespark_logs (name,log,date) VALUES ('prize','{$request}',NOW())");

    }
}
