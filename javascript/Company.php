<?php

/**
 * GameSparks
 */
class CompanyJS
{
    private $companyID;
    private $companyName;
    private $phoneNumber;
    private $street1;
    private $street2;
    private $city;
    private $state;
    private $zipCode;
    private $jsondata;

    function __construct()
    {

        // echo "hello?";
        // exit();
        global $wpdb;

        //values to vars
        $this->companyName = $_POST['companyName'];
        $this->phoneNumber = $_POST['phoneNumber'];
        $this->street1 = $_POST['street1'];
        $this->street2 = $_POST['street2'];
        $this->city = $_POST['city'];
        $this->state = $_POST['state'];
        $this->zipCode = $_POST['zipCode'];

        if ( isset($_POST['companyID']) && $_POST['companyID'] != "" ) {
            $this->companyID = $_POST['companyID'];

            //update company
            $this->UpdateCompany();
        } else {//create
            //update_post_meta($post->ID, 'company_id', $this->CreateCompany());
            $this->CreateCompany();
        }

    }

    /**
     **
     **/
    private function CreateCompany()
    {
        global $post;

        $ch = curl_init();

        $params = '{
          "@class": ".LogEventRequest",
          "eventKey": "CreateCompany",
          "companyName": "' . $this->companyName . '",
          "phoneNumber": "' . $this->phoneNumber . '",
          "street1": "' . $this->street1 . '",
          "street2": "' . $this->street2 . '",
          "city": "' . $this->city . '",
          "state": "' . $this->state . '",
          "zipCode": "' . $this->zipCode . '",
          "playerId": "' . playerID . '"
        }';

        curl_setopt($ch, CURLOPT_URL, "https://w373975yvdzr.preview.gamesparks.net/rs/device/MkfEiAZ5KZVUSpiohZF3g3GYr8iWXZFf/LogEventRequest");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        $headers[] = "Content-Type: application/json";
        $headers[] = "Accept: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        } else {
            $this->Logs($result);
            $this->jsondata = [
              "task"    => "create",
              "query"   => $params,
              "lastLog" => $result
            ];

            $result = json_decode($result);
            $this->companyID = $result->scriptData->new_company_ID;
            if( !$this->companyID ){
                $this->jsondata["status"] = "error";
            }else{
                $this->jsondata["status"] = "success";
                $this->CreatePost();
            }
            echo json_encode( $this->jsondata );
        }
        curl_close($ch);
        $this->DataBase('insert');
        //return $result->scriptData->new_company_ID;
    }

    /**
     **
     **/
    private function UpdateCompany()
    {
        global $post;

        $ch = curl_init();

        $params = '{
          "@class": ".LogEventRequest",
          "eventKey": "EditCompany2",
          "companyId": "' . $this->companyID . '",
          "companyName": "' . $this->companyName . '",
          "phoneNumber": "' . $this->phoneNumber . '",
          "street1": "' . $this->street1 . '",
          "street2": "' . $this->street2 . '",
          "city": "' . $this->city . '",
          "state": "' . $this->state . '",
          "zipCode": "' . $this->zipCode . '",
          "playerId": "' . playerID . '"
        }';

        curl_setopt($ch, CURLOPT_URL, "https://w373975yvdzr.preview.gamesparks.net/rs/device/MkfEiAZ5KZVUSpiohZF3g3GYr8iWXZFf/LogEventRequest");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        $headers[] = "Content-Type: application/json";
        $headers[] = "Accept: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        } else {
            $this->Logs($result);
            $this->jsondata = [
              "task"    => "update",
              "query"   => $params,
              "lastLog" => $result
            ];

            echo json_encode( $this->jsondata );
            $result = json_decode($result);
            //return isset($result->scriptData->update)
        }
        curl_close($ch);
        $this->DataBase("update");
    }

    /**
     **
     **/
    private function DataBase($task)
    {
        global $wpdb;

        if ($task == "insert") {
            $wpdb->query($wpdb->prepare("INSERT INTO Company (companyID,companyName,phoneNumber,street1,street2,city,state,zipCode)
                                     VALUES ('%d','%s','%s','%s','%s','%s','%s','%s')",
                array($this->companyID, $this->companyName, $this->phoneNumber, $this->street1, $this->street2, $this->city, $this->state, $this->zipCode)));
        } else if ($task == "update") {
            //check if exists on database otherwise insert again
            $res = $wpdb->get_results($wpdb->prepare("SELECT * FROM Company WHERE companyID = %d", array($this->companyID)));

            if (isset($res[0])) {
                $wpdb->get_results($wpdb->prepare("UPDATE Company
                                             SET companyName = '%s', phoneNumber = '%s', street1 = '%s',street2 = '%s', city = '%s', state = '%s', zipCode = '%s'
                                             WHERE companyID = %d", array($this->companyName, $this->phoneNumber, $this->street1, $this->street2, $this->city, $this->state, $this->zipCode, $this->companyID)));
            } else {
                $wpdb->query($wpdb->prepare("INSERT INTO Company (companyID,companyName,phoneNumber,street1,street2,city,state,zipCode)
                                       VALUES ('%d','%s','%s','%s','%s','%s','%s','%s')",
                    array($this->companyID, $this->companyName, $this->phoneNumber, $this->street1, $this->street2, $this->city, $this->state, $this->zipCode)));
            }

        }

    }

    /**
     ** Logs from gamesparks api
     **/
    private function Logs($request)
    {
        global $wpdb;

        $wpdb->query("INSERT INTO Gamespark_logs (name,log,date) VALUES ('company','{$request}',NOW())");

    }

    /**
    ** Create Post and delete all the draft posts
    **/
    private function CreatePost()
    {
        global $wpdb;
        // Create post object
        $my_post = array(
            'post_title'    => $this->companyName,
            'post_status'   => 'publish',
            'post_type'     => 'company',
            'post_author'   => get_current_user_id()
        );

        // Insert the post into the database
        $post_id = wp_insert_post( $my_post );

        update_post_meta( $post_id, 'phoneNumber', $this->phoneNumber );
        update_post_meta( $post_id, 'city', $this->city );
        update_post_meta( $post_id, 'street1', $this->street1 );
        update_post_meta( $post_id, 'street2', $this->street2 );
        update_post_meta( $post_id, 'state', $this->state );
        update_post_meta( $post_id, 'zipCode', $this->zipCode );
        update_post_meta( $post_id, 'company_id', $this->companyID );
        update_post_meta( $post_id, 'box_log', $this->jsondata["lastLog"] );
        update_post_meta( $post_id, 'query_log', $this->jsondata["query"] );

        //delete draft posts
        $wpdb->query("DELETE FROM {$wpdb->prefix}posts WHERE post_status = 'draft' AND post_type <> 'page'");

        $this->jsondata["redirect"] = get_site_url() . "/wp-admin/post.php?post={$post_id}&action=edit";
    }
}
