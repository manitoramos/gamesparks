<?php

$trashpost = 0;

add_action("admin_init", "custom_metabox");

function custom_metabox( ){
    add_meta_box( "custom_metabox_0111", "Company ID", "custom_metabox_field", "company", "side", "low" );
}

function custom_metabox_field(){
    global $post;

    $data = get_post_custom( $post->ID );
    $val = isset( $data['company_id'] ) ? esc_attr( $data['company_id'][0] ) : null;
    echo "<small>Automatic Generated after publish the post</small>";
    echo "<input type='text' name='company_id' id='company_id' style='width:100%;' value='{$val}' disabled />";
    //echo "&nbsp;<button id='savepost' style='float:right;margin-top:10px;' class='button button-primary button-large'>Save Post</button>";
}

/**
** When the trash action is fired deny the save post
**/
add_action( "wp_trash_post", "trash_post" );

function trash_post()
{
  global $trashpost;
  $trashpost++;
}



/**
** Save the post
**/
//add_action( "save_post_company","save_detail" );

function save_detail( ){
    global $post;
    global $trashpost;

    //on untrash didnt make the post function
    if( isset($_GET['action']) && $_GET['action'] == 'untrash' ){
      $previous = "javascript:history.go(-1)";
      if(isset($_SERVER['HTTP_REFERER'])) {
          $previous = $_SERVER['HTTP_REFERER'];
      }
      header('Location: '.$previous);
      return;
    }

    if($trashpost == 1){
      return;
    }

    if ( isset($post->post_status) && 'trash' === $post->post_status ) {
        return;
    }
    //Logs(json_encode($post));

    static $updated = false;

    if( $updated ) {
      return;
    }

    //if the post is not created yet
    if( !isset( $post ) ){
      return;
    }

    if( define('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
      return $post->ID;
    }

    if( isset( $post->ID ) ){

        $updated = true;

        //user and password = request 5c2ce9c9afaa2d05026e904e
        //update_post_meta( $post->ID, 'company_id', CreateCompany( $post ) );
        //new Company();

    }
}

function Logs($request)
{
  global $wpdb;

  $wpdb->query( "INSERT INTO Gamespark_logs (name,log,date) VALUES ('company','{$request}',NOW())" );

}
//
// function CreateCompany( $post )
// {
//   $ch = curl_init();
//
//   $params = '{
//     "@class": ".LogEventRequest",
//     "eventKey": "CreateCompany",
//     "companyName": "'. $post->post_title .'",
//     "phoneNumber": "'. get_post_meta( $post->ID, 'phoneNumber', true ) .'",
//     "street1": "'. get_post_meta( $post->ID, 'street1', true ) .'",
//     "street2": "'. get_post_meta( $post->ID, 'street2', true ) .'",
//     "city": "'. get_post_meta( $post->ID, 'city', true ) .'",
//     "state": "'. get_post_meta( $post->ID, 'state', true ) .'",
//     "zipCode": "'. get_post_meta( $post->ID, 'zipCode', true ) .'",
//     "playerId": "'.playerID.'"
//   }';
//
//   curl_setopt($ch, CURLOPT_URL, "https://w373975yvdzr.preview.gamesparks.net/rs/device/MkfEiAZ5KZVUSpiohZF3g3GYr8iWXZFf/LogEventRequest");
//   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//   curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
//   curl_setopt($ch, CURLOPT_POST, 1);
//
//   $headers = array();
//   $headers[] = "Content-Type: application/json";
//   $headers[] = "Accept: application/json";
//   curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//
//   $result = curl_exec($ch);
//   if (curl_errno($ch)) {
//       echo 'Error:' . curl_error($ch);
//   }else{
//     $result = json_decode($result);
//     return $result->scriptData->new_company_ID;
//   }
//   curl_close ($ch);
//   //addScore();
// }

/**
** Internal DATABASE
**/
// function InternalBD()
// {
//
// }

/**
 * GameSparks
 */
class Company
{
  private $companyID;
  private $companyName;
  private $phoneNumber;
  private $street1;
  private $street2;
  private $city;
  private $state;
  private $zipCode;

  function __construct()
  {
    global $post;

    //values to vars
    $this->companyName = $post->post_title;
    $this->phoneNumber = get_post_meta( $post->ID, 'phoneNumber', true );
    $this->street1     = get_post_meta( $post->ID, 'street1', true );
    $this->street2     = get_post_meta( $post->ID, 'street2', true );
    $this->city        = get_post_meta( $post->ID, 'city', true );
    $this->state       = get_post_meta( $post->ID, 'state', true );
    $this->zipCode     = get_post_meta( $post->ID, 'zipCode', true );

    if( get_post_meta( $post->ID, 'company_id', true ) !== null && get_post_meta( $post->ID, 'company_id', true ) !== "" )
    {
      $this->companyID = get_post_meta( $post->ID, 'company_id', true );

      //update company
      $this->UpdateCompany();
    }else{//create
      update_post_meta( $post->ID, 'company_id', $this->CreateCompany() );
    }

  }

  /**
  **
  **/
  private function CreateCompany()
  {
    $ch = curl_init();

    $params = '{
      "@class": ".LogEventRequest",
      "eventKey": "CreateCompany",
      "companyName": "'. $this->companyName .'",
      "phoneNumber": "'. $this->phoneNumber .'",
      "street1": "'. $this->street1 .'",
      "street2": "'. $this->street2 .'",
      "city": "'. $this->city .'",
      "state": "'. $this->state .'",
      "zipCode": "'. $this->zipCode .'",
      "playerId": "'.playerID.'"
    }';

    curl_setopt($ch, CURLOPT_URL, "https://w373975yvdzr.preview.gamesparks.net/rs/device/MkfEiAZ5KZVUSpiohZF3g3GYr8iWXZFf/LogEventRequest");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_POST, 1);

    $headers = array();
    $headers[] = "Content-Type: application/json";
    $headers[] = "Accept: application/json";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }else{
      $this->Logs($result);
      $result = json_decode($result);
    }
    curl_close ($ch);
    $this->DataBase('insert');
    return $result->scriptData->new_company_ID;
  }

  /**
  **
  **/
  private function UpdateCompany()
  {
    $ch = curl_init();

    $params = '{
      "@class": ".LogEventRequest",
      "eventKey": "EditCompany2",
      "companyId": "'. $this->companyID .'",
      "companyName": "'. $this->companyName .'",
      "phoneNumber": "'. $this->phoneNumber .'",
      "street1": "'. $this->street1 .'",
      "street2": "'. $this->street2 .'",
      "city": "'. $this->city .'",
      "state": "'. $this->state .'",
      "zipCode": "'. $this->zipCode .'",
      "playerId": "'.playerID.'"
    }';

    curl_setopt($ch, CURLOPT_URL, "https://w373975yvdzr.preview.gamesparks.net/rs/device/MkfEiAZ5KZVUSpiohZF3g3GYr8iWXZFf/LogEventRequest");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_POST, 1);

    $headers = array();
    $headers[] = "Content-Type: application/json";
    $headers[] = "Accept: application/json";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }else{
      $this->Logs($result);
      $result = json_decode($result);
      //return isset($result->scriptData->update)
    }
    curl_close ($ch);
    $this->DataBase("update");
  }

  /**
  **
  **/
  private function DataBase( $task )
  {
    global $wpdb;

    if( $task == "insert" ){
      $wpdb->query( $wpdb->prepare( "INSERT INTO Company (companyID,companyName,phoneNumber,street1,street2,city,state,zipCode)
                                         VALUES ('%d','%s','%s','%s','%s','%s','%s','%s')",
                                         array($this->companyID,$this->companyName,$this->phoneNumber,$this->street1,$this->street2,$this->city,$this->state,$this->zipCode) ) );
    }else if( $task == "update" ){
      //check if exists on database otherwise insert again
      $res = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM Company WHERE companyID = %d",array($this->companyID) ) );

      if( sizeof($res) > 0 ){
        $wpdb->get_results( $wpdb->prepare( "UPDATE Company
                                             SET companyName = '%s', phoneNumber = '%s', street1 = '%s',street2 = '%s', city = '%s', state = '%s', zipCode = '%s'
                                             WHERE companyID = %d",array($this->companyName,$this->phoneNumber,$this->street1,$this->street2,$this->city,$this->state,$this->zipCode,$this->companyID) ) );
      }else{
        $wpdb->query( $wpdb->prepare( "INSERT INTO Company (companyID,companyName,phoneNumber,street1,street2,city,state,zipCode)
                                           VALUES ('%d','%s','%s','%s','%s','%s','%s','%s')",
                                           array($this->companyID,$this->companyName,$this->phoneNumber,$this->street1,$this->street2,$this->city,$this->state,$this->zipCode) ) );
      }

    }

  }

  /**
  ** Logs from gamesparks api
  **/
  private function Logs($request)
  {
    global $wpdb;

    $wpdb->query( "INSERT INTO Gamespark_logs (name,log,date) VALUES ('company','{$request}',NOW())" );

  }
}
