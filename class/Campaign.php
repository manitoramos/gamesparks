<?php

/**
 * GameSparks
 */
class Campaign extends ControllerSparks
{
    private $campaignID;
    private $companyID;
    private $globalGroupRule;
    private $numQualifies;
    private $numGroups;
    private $customName;
    private $customValue;
    private $rule;

    function __construct()
    {
        $this->Configs( "prod" );

        global $post;

        //values to vars
        $this->companyID = $this->GetCompanyID();
        $this->globalGroupRule = get_field('globalGroupRule');
        $this->Logs($this->globalGroupRule);

        $this->CheckRule($this->globalGroupRule);

        if (get_field('campaign_id') !== null && get_field('campaign_id') !== "") {
            $this->campaignID = get_field('campaign_id');

            //update geolocation
            $this->UpdateCampaign();
        } else {//create
            update_post_meta($post->ID, 'campaign_id', $this->CreateCampaign());
            //exit();
        }

    }

    /**
     **
     **/
    private function CreateCampaign()
    {
        global $post;

        $ch = curl_init();

        $params = '{
          "@class": ".LogEventRequest",
          "eventKey": "CreateCampaign",
          "companyID": "' . $this->companyID . '",
          "globalGroupRule": "' . $this->rule . '",
          "playerId": "' . playerID . '"
        }';

        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        $headers[] = "Content-Type: application/json";
        $headers[] = "Accept: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        } else {
            $this->Logs($result);
            update_post_meta($post->ID, 'box_log', $result);
            update_post_meta( $post->ID, 'query_log', $params );
            $result = json_decode($result);
        }
        curl_close($ch);
        if ( isset($result->scriptData->new_campaign_ID) ) {
            $this->DataBase('insert');
            $this->campaignID = intval($result->scriptData->new_campaign_ID);
            return $result->scriptData->new_campaign_ID;
        }else{
          $my_post = array(
              'ID'           => $post->ID,
              'post_status'   => 'error',
          );
          wp_update_post( $my_post );
        }
    }

    /**
     **
     **/
    private function UpdateCampaign()
    {
        global $post;

        try {
            $ch = curl_init();

            $params = '{
              "@class": ".LogEventRequest",
              "eventKey": "EditCampaign",
              "campaignID": "' . $this->campaignID . '",
              "companyID": "' . $this->companyID . '",
              "globalGroupRule": "' . $this->rule . '",
              "playerId": "' . playerID . '"
            }';

            curl_setopt($ch, CURLOPT_URL, $this->url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_POST, 1);

            $headers = array();
            $headers[] = "Content-Type: application/json";
            $headers[] = "Accept: application/json";
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
                $this->Logs(curl_error($ch));
            } else {
                $this->Logs($result);
                update_post_meta($post->ID, 'box_log', $result);
                update_post_meta( $post->ID, 'query_log', $params );
                $result = json_decode($result);
                //return isset($result->scriptData->update)
            }
            curl_close($ch);
            if( $result->scriptData->update == "success" ){
                $this->DataBase("update");
            } else{
                //on error put the data to the same old value
            }

        } catch (Exception $e) {
            $this->Logs($e);
        }
    }

    /**
     **
     **/
    private function DataBase($task)
    {
        global $wpdb;

        if ($task == "insert") {
            $wpdb->query($wpdb->prepare("INSERT INTO Campaign (campaignID,companyID,globalGroupRule,numQualifies,numGroups,custom,dateTimeCreated,dateTimeModified)
                                     VALUES ('%d','%d','%s','%s','%s','%s')",
                array($this->campaignID, $this->companyID, $this->globalGroupRule, $this->numQualifies, $this->numGroups, $this->rule, date('Y-m-d H:i:s'), date('Y-m-d H:i:s'))));
        } else if ($task == "update") {
            //check if exists on database otherwise insert again
            $res = $wpdb->get_results($wpdb->prepare("SELECT * FROM Campaign WHERE campaignID = %d", array($this->campaignID)));
            if (isset($res[0])) {
                $wpdb->get_results($wpdb->prepare("UPDATE Campaign
                                             SET companyID = '%d', globalGroupRule = '%s', dateTimeModified = '%s', numQualifies = '%s', numGroups = '%s', custom = '%s'
                                             WHERE campaignID = %d", array($this->companyID, $this->globalGroupRule, date('Y-m-d H:i:s'), $this->numQualifies, $this->numGroups, $this->rule, $this->campaignID)));
            } else {
                $wpdb->query($wpdb->prepare("INSERT INTO Campaign (campaignID,companyID,globalGroupRule,numQualifies,numGroups,custom,dateTimeCreated,dateTimeModified)
                                       VALUES ('%d','%d','%s','%s','%s','%s')",
                    array($this->campaignID, $this->companyID, $this->globalGroupRule, $this->numQualifies, $this->numGroups, $this->rule, date('Y-m-d H:i:s'), date('Y-m-d H:i:s'))));
            }

        }

    }

    /**
     **
     **/
    private function CheckRule($rule)
    {
        global $post;

        if ($rule == "custom") {
            $this->customName = get_field('custom_name');
            $this->customValue = get_field('custom_value');
            $this->rule = "custom:[{$this->customName},{$this->customValue}]";
        } else if ($rule == "_groups_") {
            $this->numGroups = get_field('numgroups');
            $this->numQualifies = get_field('numQualifies');
            $this->rule = "{$this->numGroups}_groups_{$this->numQualifies}";
        } else if ($rule == "each_group_") {
            $this->numQualifies = get_field('numQualifies');
            $this->rule = "each_group_{$this->numQualifies}";
        }
    }

    private function GetCompanyID()
    {

        global $wpdb;
        global $post;

        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}postmeta WHERE post_id = {$post->ID} AND meta_key = 'company_campaign'");

        $value = $res[0]->meta_value;

        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}postmeta WHERE post_id = {$value} AND meta_key = 'company_id'");

        return $res[0]->meta_value;

    }

    /**
     ** Logs from gamesparks api
     **/
    private function Logs($request)
    {
        global $wpdb;

        $wpdb->query("INSERT INTO Gamespark_logs (name,log,date) VALUES ('campaign','{$request}',NOW())");

    }
}
