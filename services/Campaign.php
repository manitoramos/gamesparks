<?php

add_action("admin_init", "campaign_custom_metabox");

function campaign_custom_metabox( ){
    add_meta_box( "campaign", "Campaign ID", "campaign_metabox", "campaign", "side", "low" );
}

function campaign_metabox(){
    global $post;

    $data = get_post_custom( $post->ID );
    $val = isset( $data['campaign_id'] ) ? esc_attr( $data['campaign_id'][0] ) : null;
    echo "<small>Automatic Generated after publish the post</small>";
    echo "<input type='text' name='campaign_id' id='campaign_id' style='width:100%;' value='{$val}' disabled />";
}


/**
** Save Post
**/
add_action( "save_post_campaign","save_campaign" );

function save_campaign( ){
    global $post;
    global $trashpost;

    //on untrash didnt make the post function
    if( isset($_GET['action']) && $_GET['action'] == 'untrash' ){
      $previous = "javascript:history.go(-1)";
      if(isset($_SERVER['HTTP_REFERER'])) {
          $previous = $_SERVER['HTTP_REFERER'];
      }
      header('Location: '.$previous);
      return;
    }

    if($trashpost == 1){
      return;
    }

    static $updated = false;

    if( $updated ) {
      return;
    }

    //if the post is not created yet
    if( !isset( $post ) ){
      return;
    }

    // if( define('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
    //   return $post->ID;
    // }
    if( isset( $post->ID ) ){

        $updated = true;

        new Campaign();

    }
}


/**
 * GameSparks
 */
class Campaign
{
  private $campaignID;
  private $companyID;
  private $globalGroupRule;
  private $numQualifies;
  private $numGroups;
  private $customName;
  private $customValue;
  private $rule;

  function __construct()
  {
    global $post;

    //values to vars
    $this->companyID = get_post_meta( $post->ID, 'company_campaign', true );
    $this->globalGroupRule= get_post_meta( $post->ID, 'globalGroupRule', true );
    $this->Logs($this->globalGroupRule);

    $this->CheckRule( $this->globalGroupRule );

    if( get_post_meta( $post->ID, 'campaign_id', true ) !== null && get_post_meta( $post->ID, 'campaign_id', true ) !== "" )
    {
      $this->campaignID = get_post_meta( $post->ID, 'campaign_id', true );

      //update geolocation
      $this->UpdateCampaign();
    }else{//create
      update_post_meta( $post->ID, 'campaign_id', $this->CreateCampaign() );
      //exit();
    }

  }

  /**
  **
  **/
  private function CreateCampaign()
  {
    $ch = curl_init();

    $params = '{
      "@class": ".LogEventRequest",
      "eventKey": "CreateCampaign",
      "companyID": "'. 2 .'",
      "globalGroupRule": "'. $this->rule .'",
      "playerId": "'.playerID.'"
    }';

    curl_setopt($ch, CURLOPT_URL, "https://w373975yvdzr.preview.gamesparks.net/rs/device/MkfEiAZ5KZVUSpiohZF3g3GYr8iWXZFf/LogEventRequest");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_POST, 1);

    $headers = array();
    $headers[] = "Content-Type: application/json";
    $headers[] = "Accept: application/json";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }else{
      $this->Logs($result);
      $result = json_decode($result);
    }
    curl_close ($ch);
    if( isset($result->scriptData->new_campaign_ID) ){
      $this->DataBase('insert');
      $this->campaignID = intval($result->scriptData->new_campaign_ID);
      return $result->scriptData->new_campaign_ID;
    }
  }

  /**
  **
  **/
  private function UpdateCampaign()
  {
    try {
      $ch = curl_init();

      $params = '{
        "@class": ".LogEventRequest",
        "eventKey": "EditCampaign",
        "campaignID": "'. $this->campaignID .'",
        "companyID": "'. 2 .'",
        "globalGroupRule": "'. $this->rule .'",
        "playerId": "'.playerID.'"
      }';

      curl_setopt($ch, CURLOPT_URL, "https://w373975yvdzr.preview.gamesparks.net/rs/device/MkfEiAZ5KZVUSpiohZF3g3GYr8iWXZFf/LogEventRequest");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      curl_setopt($ch, CURLOPT_POST, 1);

      $headers = array();
      $headers[] = "Content-Type: application/json";
      $headers[] = "Accept: application/json";
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      $result = curl_exec($ch);
      if (curl_errno($ch)) {
          echo 'Error:' . curl_error($ch);
          $this->Logs(curl_error($ch));
      }else{
        //return isset($result->scriptData->update)
      }
      curl_close ($ch);
      $this->Logs($result);
      $result = json_decode($result);
      $this->DataBase("update");
    } catch (Exception $e) {
      $this->Logs($e);
    }
  }

  /**
  **
  **/
  private function DataBase( $task )
  {
    global $wpdb;

    if( $task == "insert" ){
      $wpdb->query( $wpdb->prepare( "INSERT INTO Campaign (campaignID,companyID,globalGroupRule,dateTimeCreated,dateTimeModified)
                                         VALUES ('%d','%d','%s','%s','%s')",
                                         array($this->campaignID,$this->companyID,$this->globalGroupRule,date('Y-m-d H:i:s'),date('Y-m-d H:i:s')) ) );
    }else if( $task == "update" ){
      //check if exists on database otherwise insert again
      $res = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM Campaign WHERE campaignID = %d",array($this->geoID) ) );

      if( sizeof($res) > 0 ){
        $wpdb->get_results( $wpdb->prepare( "UPDATE Campaign
                                             SET companyID = '%d', globalGroupRule = '%s', dateTimeModified = '%s'
                                             WHERE campaignID = %d",array($this->companyID,$this->globalGroupRule,date('Y-m-d H:i:s'),$this->campaignID) ) );
      }else{
        $wpdb->query( $wpdb->prepare( "INSERT INTO Campaign (campaignID,companyID,globalGroupRule,dateTimeCreated,dateTimeModified)
                                           VALUES ('%d','%d','%s','%s','%s')",
                                           array($this->campaignID,$this->companyID,$this->globalGroupRule,date('Y-m-d H:i:s'),date('Y-m-d H:i:s')) ) );
      }

    }

  }

  /**
  **
  **/
  private function CheckRule( $rule )
  {
    global $post;

    if( $rule == "custom" ){
      $this->customName = get_post_meta( $post->ID, 'custom_name', true );
      $this->customValue = get_post_meta( $post->ID, 'custom_value', true );
      $this->rule = "custom:[{$this->customName},{$this->customValue}]";
    }else if( $rule == "_groups_" ){
      $this->numGroups = get_post_meta( $post->ID, 'numgroups', true );
      $this->numQualifies = get_post_meta( $post->ID, 'numQualifies', true );
      $this->rule = "{$this->numGroups}_groups_{$this->numQualifies}";
    }else if( $rule == "each_group_" ){
      $this->numQualifies = get_post_meta( $post->ID, 'numQualifies', true );
      $this->rule = "each_group_{$this->numQualifies}";
    }
  }

  /**
  ** Logs from gamesparks api
  **/
  private function Logs($request)
  {
    global $wpdb;

    $wpdb->query( "INSERT INTO Gamespark_logs (name,log,date) VALUES ('campaign','{$request}',NOW())" );

  }
}
